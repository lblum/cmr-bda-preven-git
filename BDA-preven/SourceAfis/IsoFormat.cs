﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Drawing;

namespace SourceAFIS.Templates
{
    public class IsoFormat
    {
        // References:
        // http://www.italdata-roma.com/PDF/Norme%20ISO-IEC%20Minutiae%20Data%20Format%2019794-2.pdf
        // https://biolab.csr.unibo.it/fvcongoing/UI/Form/Download.aspx (ISO section, sample ZIP, ISOTemplate.pdf)
        //
        // Format (all numbers are big-endian):
        // 4B magic "FMR\0"
        // 4B version (ignored, set to " 20\0"
        // 4B total length (including header)
        // 2B rubbish (zeroed)
        // 2B image size in pixels X
        // 2B image size in pixels Y
        // 2B rubbish (pixels per cm X, set to 196 = 500dpi)
        // 2B rubbish (pixels per cm Y, set to 196 = 500dpi)
        // 1B rubbish (number of fingerprints, set to 1)
        // 1B rubbish (zeroed)
        // 1B rubbish (finger position, zeroed)
        // 1B rubbish (zeroed)
        // 1B rubbish (fingerprint quality, set to 100)
        // 1B minutia count
        // N*6B minutiae
        //      2B minutia position X in pixels
        //          2b (upper) minutia type (01 ending, 10 bifurcation, 00 other)
        //      2B minutia position Y in pixels (upper 2b ignored, zeroed)
        //      1B direction, compatible with SourceAFIS angles
        //      1B quality (ignored, zeroed)
        // 2B rubbish (extra data length, zeroed)
        // N*1B rubbish (extra data)

        public string Format { get; private set; }
        public string Version { get; private set; }
        public int Length { get; private set; }
        public int ImgWidth { get; private set; }
        public int ImgHeight { get; private set; }
        public int Quality { get; private set; }
        public int MinutiaCount { get; private set; }
        public Minutia[] Minutiae { get; private set; }
        public Point Centroid;


        public IsoFormat(byte[] template)
        {
            MemoryStream stream = new MemoryStream(template);
            BinaryReader reader = new BinaryReader(stream, Encoding.UTF8);

            // 4B magic "FMR\0"
            Format = new string(reader.ReadChars(4));

            // 4B version (ignored, set to " 20\0"
            Version = new string(reader.ReadChars(4));

            // 4B total length (including header)
            Length = IPAddress.NetworkToHostOrder(reader.ReadInt32());

            // 2B rubbish (zeroed)
            reader.ReadInt16();

            // 2B image size in pixels X
            ImgWidth = IPAddress.NetworkToHostOrder(reader.ReadInt16());

            // 2B image size in pixels Y
            ImgHeight = IPAddress.NetworkToHostOrder(reader.ReadInt16());

            // 2B rubbish (pixels per cm X, set to 196 = 500dpi)
            reader.ReadInt16();

            // 2B rubbish (pixels per cm Y, set to 196 = 500dpi)
            reader.ReadInt16();

            // 1B rubbish (number of fingerprints, set to 1)
            reader.ReadByte();

            // 1B rubbish (zeroed)
            reader.ReadByte();

            // 1B rubbish (finger position, zeroed)
            reader.ReadByte();

            // 1B rubbish (zeroed)
            reader.ReadByte();

            // 1B rubbish (fingerprint quality, set to 100)
            Quality = reader.ReadByte();

            // 1B minutia count
            MinutiaCount = reader.ReadByte();
            Minutiae = new Minutia[MinutiaCount];
            Centroid = new Point(0, 0);

            for (int i = 0; i < MinutiaCount; ++i)
            {
                Minutia m = new Minutia();
                //      2B minutia position X in pixels
                //          2b (upper) minutia type (01 ending, 10 bifurcation, 00 other (considered ending))
                ushort xPacked = (ushort)IPAddress.NetworkToHostOrder(reader.ReadInt16());
                m.X = xPacked & (ushort)0x3fff;
                switch (xPacked & (ushort)0xc000)
                {
                    case 0x4000: m.Type = MinutiaType.Ending; break;
                    case 0x8000: m.Type = MinutiaType.Bifurcation; break;
                    case 0: m.Type = MinutiaType.Other; break;
                }
                //      2B minutia position Y in pixels (upper 2b ignored, zeroed)
                m.Y = ImgHeight - 1 - ((ushort)IPAddress.NetworkToHostOrder(reader.ReadInt16()) & (ushort)0x3fff);
                //      1B direction, compatible with SourceAFIS angles
                m.Angle = reader.ReadByte();
                //      1B quality (ignored, zeroed)
                m.Quality = reader.ReadByte();
                Minutiae[i] = m;
                Centroid.X += m.X;
                Centroid.Y += m.Y;
            }
            if (MinutiaCount > 0)
            {
                Centroid.X /= MinutiaCount;
                Centroid.Y /= MinutiaCount;
            }

            // 2B rubbish (extra data length, zeroed)
            // N*1B rubbish (extra data)

        }

    }

    public class Minutia
    {
        public MinutiaType Type;
        public int X;
        public int Y;
        public int Angle;
        public int Quality;
    }

    public enum MinutiaType
    {
        Ending = 0,
        Bifurcation = 1,
        Other = 2
    }
}
