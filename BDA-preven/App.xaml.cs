﻿using BDAPreven.BarCode;
using BDAPreven.Singleton;
using System;
using System.Drawing;
using System.Windows;

namespace BDAPreven
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App() 
        {
            
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Splash splashWindow = new Splash();
            splashWindow.mainWindow = new MainWindow();
            Application.Current.MainWindow = splashWindow;
            //Application.Current.MainWindow = new TestWindow();
            Application.Current.MainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }

    }
}
