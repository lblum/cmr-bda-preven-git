﻿/*
  SQL Server
*/
DROP TABLE [dbo].[dni]
GO
CREATE TABLE [dbo].[dni](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ts] [datetime] NULL CONSTRAINT [DF_fp_ts]  DEFAULT (getdate()),
	[id_user] [varchar](max) NULL DEFAULT CURRENT_USER,
	[img1] [varbinary](max) NULL,
	[img2] [varbinary](max) NULL,
	[pdf_417_data] [varchar](max) NULL,
	[dni] [varchar](10) NULL,
	[apellidos] [varchar](max) NULL,
	[nombres] [varchar](max) NULL,
	[sexo] [varchar](max) NULL,
	[nacionalidad] [varchar](max) NULL,
	[fnacimiento] [date] NULL,
	[ok] int not null default 1,
	[scdata] text,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
)
GO
drop table [fp]
go
CREATE TABLE [fp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_dni] [int] NOT NULL,
	[img]  [varbinary](max) NULL,
	[tpl]  [varbinary](max) NULL,
	[ok] int not null default 0,
	[fmt] int,
	[version] text,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
)
GO
/*
  MySQL
*/
DROP TABLE `dni`;
DROP TABLE `fp`;

CREATE TABLE `dni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` text,
  `img1` mediumblob,
  `img2` mediumblob,
  `pdf_417_data` text,
  `dni` varchar(10) DEFAULT NULL,
  `apellidos` text,
  `nombres` text,
  `sexo` text,
  `nacionalidad` text,
  `fnacimiento` date DEFAULT NULL,
  `ok` int(11) NOT NULL DEFAULT '1',
  `scdata` text,
  `dpi` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `fp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dni` int(11) NOT NULL,
  `img` mediumblob,
  `tpl` mediumblob,
  `ok` int(11) NOT NULL DEFAULT '0',
  `fmt` int(11),
  `version` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TRIGGER `ins_dni` BEFORE INSERT ON dni FOR EACH ROW
BEGIN
  set new.id_user = current_user();
END;

