﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Windows.Media.Imaging;

namespace BDAPreven.BIO
{
    public class ImgUtils
    {
        public static BitmapSource ToWpfBitmap(Bitmap bitmap)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Bmp);

                stream.Position = 0;
                BitmapImage result = new BitmapImage();
                result.BeginInit();
                // According to MSDN, "The default OnDemand cache option retains access to the stream until the image is needed."
                // Force the bitmap to load right now so we can dispose the stream.
                result.CacheOption = BitmapCacheOption.OnLoad;
                result.StreamSource = stream;
                result.EndInit();
                result.Freeze();
                return result;
            }
        }

        public static Bitmap scaleBitmap(Bitmap bmp, int nPorc)
        {
            int newW;
            int newH;
            int newX;
            int newY;
            float r = (100F - (float)nPorc) / 100F;
            Bitmap bmp1, bmp2;

            newW = (int)((float)bmp.Width * r);
            newH = (int)((float)bmp.Height * r);

            newX = (bmp.Width - newW);
            newY = (bmp.Height - newH);
            bmp1 = bmp.Clone(new Rectangle(newX, newY, newW, newH), bmp.PixelFormat);
            bmp2 = new Bitmap(bmp1, new System.Drawing.Size(bmp.Width, bmp.Height));
            return bmp2;
        }


        public static Bitmap rawToBitmap(byte[] RawData,int Width,int Height)
        {
            byte[] rgbBytes = new byte[RawData.Length * 3];

            for (int i = 0; i <= RawData.Length - 1; i++)
            {
                rgbBytes[(i * 3)] = RawData[i];
                rgbBytes[(i * 3) + 1] = RawData[i];
                rgbBytes[(i * 3) + 2] = RawData[i];
            }

            Bitmap BMP = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);

            BitmapData data = BMP.LockBits(new Rectangle(0, 0, BMP.Width, BMP.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            for (int i = 0; i <= BMP.Height - 1; i++)
            {
                IntPtr p = new IntPtr(data.Scan0.ToInt64() + data.Stride * i);
                System.Runtime.InteropServices.Marshal.Copy(rgbBytes, i * BMP.Width * 3, p, BMP.Width * 3);
            }

            BMP.UnlockBits(data);

            return BMP;
        }

        public static object imageToByteArrayGZip(Bitmap bmp)
        {
            byte[] ba = imageToByteArray(bmp);
            byte[] retVal = null;

            using (MemoryStream ms = new MemoryStream())
            {
                using (DeflateStream gzip = new DeflateStream(ms, CompressionMode.Compress, true))
                {
                    gzip.Write(ba, 0, ba.Length);
                    gzip.Close();
                }

                retVal = ms.ToArray();
            }

            return retVal;

        }

        public static byte[] bitmapToRaw(Bitmap bmp)
        {
            byte[] res = new byte[bmp.Height * bmp.Width];
            BitmapData srcData = null; ;
            try
            {
                srcData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
                int bpp = 4; ;
                unsafe
                {
                    int i = 0;
                    byte* srcPointer = (byte*)srcData.Scan0;
                    int f;
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        for (int x = 0; x < bmp.Width; x++)
                        {
                            f = srcPointer[1] + srcPointer[2] + srcPointer[3];
                            res[i++] = (byte)(f / 3);
                            srcPointer += bpp;
                        }
                    }
                }
            }
            catch (InvalidOperationException)
            {

            }
            finally
            {
                bmp.UnlockBits(srcData);
            }

             return res;
        }

        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            return imageToByteArray(imageIn, "BMP");
        }

        public static byte[] imageToByteArrayFmt(System.Drawing.Image imageIn)
        {
            return imageToByteArray(imageIn, Properties.Settings.Default.imgFmt);
        }

        private static ImageCodecInfo GetEncoder(string format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatDescription.ToUpper() == format.ToUpper() )
                {
                    return codec;
                }
            }
            return null;
        }

        public static byte[] imageToByteArray(System.Drawing.Image imageIn,string fmt)
        {
            if (imageIn == null)
                return null;
            MemoryStream ms = new MemoryStream();
            ImageCodecInfo enc = GetEncoder(fmt);
            EncoderParameters ecs = new EncoderParameters(1);
            System.Drawing.Imaging.Encoder myEncoder =
                System.Drawing.Imaging.Encoder.Quality;
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, Properties.Settings.Default.imgQ);
            ecs.Param[0] = myEncoderParameter;

            imageIn.Save(ms, enc, ecs);
            return ms.ToArray();
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static byte[] getRawData(Image<Gray, byte> img)
        {
            byte[] retVal = new byte[img.Width * img.Height];
            int i = 0;
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    retVal[i++] = img.Data[y, x, 0];
                }
            }
            return retVal;
        }

    }
}
