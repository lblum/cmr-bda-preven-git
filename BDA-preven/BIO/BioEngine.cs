﻿using System;
using DPUruNet;
using System.Drawing;

namespace BDAPreven.BIO
{
    public abstract class BioEngine
    {

        public BioEngine()
        {

        }

        public virtual void startReading()
        {
        }

        public System.Windows.Controls.Control Container;

        public delegate void OnCaptureOKCallback(Fmd fmd, Bitmap bmp);

        public OnCaptureOKCallback OnCaptureOk = null;

        public virtual void resetReaders()
        {
        }
    }

}
