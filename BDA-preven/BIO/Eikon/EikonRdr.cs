﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;

namespace BDAPreven.BIO.Eikon
{
    public class EikonRdr
    {
        private const int PT_GRAB_TYPE_UPPER_HALF = 0;
        private const int PT_GRAB_TYPE_BOTTOM_HALF = 1;
        private const int PT_GRAB_TYPE_THREE_QUARTERS_SUBSAMPLE = 2;
        private const int PT_GRAB_TYPE_381_381_8 = 2;    ///< alias for PT_GRAB_TYPE_THREE_QUARTERS_SUBSAMPLE
        private const int PT_GRAB_TYPE_ONE_HALF_SUBSAMPLE = 3;
        private const int PT_GRAB_TYPE_254_254_8 = 3;    ///< alias for PT_GRAB_TYPE_ONE_HALF_SUBSAMPLE
        private const int PT_GRAB_TYPE_THREE_QUARTERS_SUBSAMPLE_BINARIZED = 4;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED = 4;    ///< alias for PT_GRAB_TYPE_THREE_QUARTERS_SUBSAMPLE_BINARIZED
        private const int PT_GRAB_TYPE_508_254_8 = 5;
        private const int PT_GRAB_TYPE_508_508_4 = 6;
        private const int PT_GRAB_TYPE_381_381_4 = 7;
        private const int PT_GRAB_TYPE_508_254_4 = 8;
        private const int PT_GRAB_TYPE_254_254_4 = 9;
        private const int PT_GRAB_TYPE_508_508_8_WIDTH208 = 10;
        private const int PT_GRAB_TYPE_508_508_8_COMPRESS1 = 11;
        private const int PT_GRAB_TYPE_508_508_4_SCAN4 = 12;
        private const int PT_GRAB_TYPE_381_381_8_FAST = 13;
        private const int PT_GRAB_TYPE_508_254_4_SCAN4 = 14;
        private const int PT_GRAB_TYPE_254_254_4_SCAN4 = 15;
        private const int PT_GRAB_TYPE_381_381_4_FAST = 16;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_FAST = 17;
        private const int PT_GRAB_TYPE_508_508_8_COMPRESS2 = 18;
        private const int PT_GRAB_TYPE_381_381_8_SCAN381 = 19;
        private const int PT_GRAB_TYPE_381_381_4_SCAN381 = 20;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_SCAN381 = 21;
        private const int PT_GRAB_TYPE_381_381_8_LP = 22;    ///< Low power
        private const int PT_GRAB_TYPE_381_381_4_LP = 23;    ///< Low power
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_LP = 24;    ///< Low power
        private const int PT_GRAB_TYPE_381_381_8_VLP = 25;    ///< Very low power
        private const int PT_GRAB_TYPE_381_381_4_VLP = 26;    ///< Very low power
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_VLP = 27;    ///< Very low power
        private const int PT_GRAB_TYPE_381_381_8_SCAN381_381_4 = 28;
        private const int PT_GRAB_TYPE_381_381_4_SCAN381_381_4 = 29;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_SCAN381_381_4 = 30;
        private const int PT_GRAB_TYPE_381_381_8_SCAN381_254_4 = 31;
        private const int PT_GRAB_TYPE_381_381_4_SCAN381_254_4 = 32;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_SCAN381_254_4 = 33;
        private const int PT_GRAB_TYPE_508_508_8_SCAN508_508_8 = 34;
        private const int PT_GRAB_TYPE_508_508_4_SCAN508_508_8 = 35;
        private const int PT_GRAB_TYPE_508_508_8_BINARIZED_SCAN508_508_8 = 36;
        private const int PT_GRAB_TYPE_508_508_8_SCAN4 = 37;
        private const int PT_GRAB_TYPE_508_508_8_BINARIZED_SCAN4 = 38;
        private const int PT_GRAB_TYPE_508_508_8_COMPRESS3 = 39;
        private const int PT_GRAB_TYPE_508_254_8_LP = 40;
        private const int PT_GRAB_TYPE_508_254_4_LP = 41;
        private const int PT_GRAB_TYPE_381_381_8_FAST_LP = 42;
        private const int PT_GRAB_TYPE_381_381_4_FAST_LP = 43;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_FAST_LP = 44;
        private const int PT_GRAB_TYPE_381_381_8_COMPRESS4 = 45;
        private const int PT_GRAB_TYPE_381_381_8_SCAN254_381_8 = 46;
        private const int PT_GRAB_TYPE_381_381_8_BINARIZED_SCAN254_381_8 = 47;
        private const int PT_GRAB_TYPE_381_381_8_SCAN381_381_6 = 48;    ///< Supports variable bandwidth, input format 381/381/6 or 254/381/6 depending on available bandwidth. Subject to 'ImageFormatPolicy' session parameter.
        private const int PT_GRAB_TYPE_381_381_8_SCAN254_381_6 = 49;    ///< Supports variable bandwidth, input format fixed to 254/381/6. Subject to 'ImageFormatPolicy' session parameter.
        private const int PT_GRAB_TYPE_IMGFORMAT = 255;

        private const int PT_CANCEL_FLAG_CLEAR_CANCEL = 0x00000001;

        [StructLayout(LayoutKind.Sequential)]
        private struct VBPT_VERSION
        {
            public Int16 Revision;
            public byte MinorVer;
            public byte MajorVer;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct PT_INFO
        {
            // Version of the TFM's firmware.Highest byte = major version,
            // second highest byte = minor version, low word = subversions/revisions.
            public VBPT_VERSION FwVersion;

            // Minimal version required for future firmware update
            public VBPT_VERSION FwMinNextVersion;

            // Variant of firmware - see PT_FWVARIANT_xxxx (E.g. variant with USB, variant with SIO etc.)
            public Int32 FwVariant;

            // Blocks of functionality included in firmware. See PT_FWFUNC_xxxx.
            public Int32 FwFunctionality;

            // FW's configuration flags, set up during manufacturing. See PT_FWCFG_xxxx.
            public Int32 FwConfig;

            // TFM ID.  If used, allows to assign unique ID to every TFM piece. Otherwise 0.
            public Int32 Id;

            // ID of the Authentify group. Every TFM with the same Authentify code
            // belongs to the same Authentify group. If AuthentifyId == 0,
            // PTAuthentify is not necessary. See PTAuthentify().
            public Int32 AuthentifyId;

            // Structure of the reader in which the TFM is used. 0 = unspecified usage.
            public Int32 Usage;

            // Structure and version of sensor.
            public Int32 SensorType;

            // Sensor image width
            public Int16 ImageWidth;

            // Sensor image height (for strip sensor max. height)
            public Int16 ImageHeight;

            // Max. guaranteed length of the output data for PTGrabWindow
            public Int32 MaxGrabWindow;

            // Companion vendor code
            public Int32 CompanionVendorCode;
        };


        [DllImport("tfm.dll")]
        private static extern int PTInitialize(IntPtr pMemoryFuncs);

        [DllImport("tfm.dll")]
        private static extern int PTTerminate();

        [DllImport("tfm.dll")]
        private static extern int PTOpen(string dsn, out Int32 conn);

        [DllImport("tfm.dll")]
        unsafe private static extern int PTInfo(Int32 conn, out IntPtr ppInfo);

        unsafe public struct PT_DATA
        {
            public Int32 Length;       ///< Length of the Data field in bytes
            public byte Data;         ///< The data itself, variable length
        };

        [DllImport("tfm.dll")]
        unsafe private static extern int PTGrab(Int32 conn, byte byType, Int32 lTimeout, Int32 boWaitForAcceptableFinger, out IntPtr ppData, Int32 pSignData, Int32 ppSignature);

        [DllImport("tfm.dll")]
        unsafe private static extern int PTCancel(Int32 conn, Int32 dwFlags);

        [DllImport("tfm.dll")]
        unsafe private static extern int PTSetGUICallbacks(Int32 conn, Int32 pfnGuiStreamingCallback, Int32 pGuiStreamingCallbackCtx, Int32 pfnGuiStateCallback, Int32 pfnGuiStateCallbackCtx);

        [DllImport("tfm.dll")]
        unsafe private static extern void PTFree(IntPtr memBlock);

        private EikonRdr()
        {
            InitRdr();
            bw = new BackgroundWorker();
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.DoWork += bw_DoScan;
        }

        ~EikonRdr()
        {
            PTTerminate();
            hConn = -1;
        }

        private static EikonRdr mainRdr = null;

        public static EikonRdr getRdr()
        {
            if (mainRdr == null)
            {
                mainRdr = new EikonRdr();
            }
            return mainRdr;
        }

        public static void releaseRdr()
        {
            mainRdr = null;
        }

        private Int32 hConn = -1;
        public int Width { get; private set; }
        public int Height { get; private set; }
        private void InitRdr()
        {
            // Inicializo el sistema de Eikon
            EikonRdrErrors.check(PTInitialize(IntPtr.Zero));

            // Me conecto al lector
            string dsn = "usb;timeout=100";
            Int32 conn;
            EikonRdrErrors.check(PTOpen(dsn, out conn));
            hConn = conn;

            // Sin los carteles default
            EikonRdrErrors.check(PTSetGUICallbacks(hConn, 0, 0, 0, 0));

            // Información del lector
            unsafe
            {
                IntPtr ppInfo;
                EikonRdrErrors.check(PTInfo(hConn, out ppInfo));
                Width = ((PT_INFO*)ppInfo)->ImageWidth;
                Height = ((PT_INFO*)ppInfo)->ImageHeight;
            }
        }

        public bool Scanning { get; private set; }
        public Bitmap lastFP { get; private set; } = null;

        public delegate void OnCaptureOKCallback(Bitmap bmp);
        public OnCaptureOKCallback OnCaptureOk = null;

        public delegate void OnCaptureFailCallback(string errStr);
        public OnCaptureFailCallback OnCaptureFail = null;

        private BackgroundWorker bw = null;

        private bool isCanceling = false;
        public void startScan()
        {
            isCanceling = false;
            bw.RunWorkerAsync();
        }

        public void stopScan()
        {
            // Debiera haber una forma mas civilizada
            isCanceling = true;
            PTTerminate();// (hConn, 0);// PT_CANCEL_FLAG_CLEAR_CANCEL);
            InitRdr();
        }

        protected void bw_DoScan(object sender, DoWorkEventArgs e)
        {
            unsafe
            {
                PT_DATA* ppData = null;
                IntPtr ppLData = IntPtr.Zero;
                try
                {
                    Scanning = true;
                    lastFP = null;
                    EikonRdrErrors.check(PTGrab(hConn, PT_GRAB_TYPE_508_508_8_SCAN508_508_8, 30000, 1, out ppLData, 0, 0));
                    ppData = (PT_DATA*)ppLData;
                    lastFP = new Bitmap(Width, Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    byte* p = &(ppData->Data);
                    for (int yy = 0; yy < Height; yy++)
                    {
                        for (int xx = 0; xx < Width; xx++)
                        {
                            int c = (int)*(p + xx + yy * Width);
                            lastFP.SetPixel(xx, yy, System.Drawing.Color.FromArgb(c, c, c));
                        }
                    }

                    Scanning = false;
                    if (OnCaptureOk != null)
                    {
                        if (OnCaptureOk.Target is System.Windows.Controls.Control)
                            (OnCaptureOk.Target as System.Windows.Controls.Control).Dispatcher.Invoke(OnCaptureOk, lastFP);
                        else
                            OnCaptureOk(lastFP);
                    }
                }
                catch (Exception scanEx)
                {
                    string str = isCanceling ? "Operación cancelada por el usuario" : scanEx.Message;
                    if (OnCaptureFail != null)
                    {
                        if (OnCaptureFail.Target is System.Windows.Controls.Control)
                            (OnCaptureFail.Target as System.Windows.Controls.Control).Dispatcher.Invoke(OnCaptureFail, str);
                        else
                            OnCaptureFail(str);
                    }
                }

                finally
                {
                    try
                    {
                        PTFree(ppLData);
                        ppData = null;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
        }
    }
}
