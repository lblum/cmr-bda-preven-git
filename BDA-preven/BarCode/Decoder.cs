﻿using System.IO;
using System.Windows.Media.Imaging;
using ZXing;
using BDAPreven.DNI;
using System.Drawing.Imaging;
using System.Drawing;
using System;
using System.Windows;

namespace BDAPreven.BarCode
{
    public class Decoder
    {
        private readonly static BarcodeReader reader = new BarcodeReader();

        public static System.Drawing.Bitmap ToWinFormsBitmap(BitmapSource bitmapsource)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(stream);

                using (var tempBitmap = new System.Drawing.Bitmap(stream))
                {
                    // According to MSDN, one "must keep the stream open for the lifetime of the Bitmap."
                    // So we return a copy of the new bitmap, allowing us to dispose both the bitmap and the stream.
                    return new System.Drawing.Bitmap(tempBitmap);
                }
            }
        }

        public static BitmapSource GetImageStream(System.Drawing.Image myImage)
        {
            var bitmap = new Bitmap(myImage);
            IntPtr bmpPt = bitmap.GetHbitmap();
            BitmapSource bitmapSource =
             System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                   bmpPt,
                   IntPtr.Zero,
                   Int32Rect.Empty,
                   BitmapSizeOptions.FromEmptyOptions());

            //freeze bitmapSource and clear memory to avoid memory leaks
            bitmapSource.Freeze();
            //DeleteObject(bmpPt);

            return bitmapSource;
        }

        public static DNI.DNI_Bio decodeImg(BitmapSource src)
        {
            Result result;
            Bitmap bmp = ToWinFormsBitmap(src);
            result = reader.Decode(bmp);

            if (result == null)
            {
                // Si no anduvo a la primera, trato de rotarlo 90, 180 y 270 grados
                System.Drawing.Bitmap bitmap = ToWinFormsBitmap((BitmapSource)src);
                for ( int i=0; i < 3; i++)
                {
                    bitmap.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                    if ((result = reader.Decode(bitmap)) != null)
                        break;
                }
            }
            if (result == null)
                return null;

            return DNIFactory.getNewInstance(result.Text);

        }

        public static byte[] getImgArray(BitmapSource src)
        {           
            MemoryStream stream = new MemoryStream();
            BitmapEncoder enc = new PngBitmapEncoder();
            enc.Frames.Add(BitmapFrame.Create(src));
            enc.Save(stream);
            byte[] retVal = stream.ToArray();
            return retVal;
        }

        public static Image fromByteArray(byte[] bArray)
        {
            MemoryStream ms = new MemoryStream(bArray);
            BitmapSource bmpS = BitmapFrame.Create(ms);

            return ToWinFormsBitmap(bmpS);
        }
    }
}
