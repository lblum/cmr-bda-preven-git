﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BDAPreven
{
    /// <summary>
    /// Interaction logic for Splash.xaml
    /// </summary>
    public partial class Splash : Window
    {
        public Splash()
        {
            InitializeComponent();
        }

        public Window mainWindow = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer tt = new DispatcherTimer();
            tt.Interval = new TimeSpan(0, 0, 3);
            tt.Tick += (s, a) =>
            {
                tt.Stop();
                Application.Current.MainWindow = mainWindow;
                Application.Current.MainWindow.Show();
                this.Hide();
            };
            tt.Start();
        }
    }
}
