﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using BDAPreven.BarCode;
using System.ComponentModel;
using BDAPreven.Singleton;
using DPUruNet;
using System.Drawing;
using NTwain;
using NTwain.Data;
using System.Reflection;

namespace BDAPreven
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string currTaskID = null;
        private bool devicePresent = false;
        
        public MainWindow()
        {
            InitializeComponent();

            initScanner();
        }

        protected bool scanOK = true;
        protected string scanTxt = "";


        public System.Windows.Controls.Image imageDest = null;
        public void getBarCodeImage()
        {
            Logger.startBarCode();
            dniNumero.Text =
            dniApellidos.Text =
            dniNombres.Text =
            dniSexo.Text =
            dniNacionalidad.Text =
            dniFechaNacimiento.Text = "";
            taskArea.CurrTaskID = "barCodeScan";
            taskArea.TaskText = "Coloque el DNI en el scanner con el código de barras hacia abajo";
            taskArea.Busy.Visibility = Visibility.Hidden;
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            imageDest = imgBarCode;

            if ((devicePresent || Properties.Settings.Default.IsInProd) && !Properties.Settings.Default.testDNI)
            {
                taskArea.GoFunc = scanImage;
            }
            else
            {
                taskArea.GoFunc = loadImage;
            }
        }

        public void scanImage()
        {
            taskArea.TaskText = "Escaneando ...";
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.Busy.Visibility = Visibility.Visible;
            try
            {
                scanOK = false;
                startScan();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                if (imageDest == imgBarCode)
                    getBarCodeImage();
                else
                    getImgOtro();
                return;
            }
        }

        private void getFP()
        {
            taskArea.CurrTaskID = "fingerCapture";
            taskArea.TaskText = "Ponga su dígito pulgar derecho en el lector de huella";
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnGo.Focus();
            taskArea.btnBack.Visibility = Visibility.Visible;
            taskArea.BackFunc = getImgOtro;
            taskArea.Busy.Visibility = Visibility.Hidden;
            taskArea.GoFunc = loadFP;
        }

        private void loadFP()
        {
            taskArea.TaskText = "Leyendo huella ...";
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.BackFunc = cancelFP;
            taskArea.btnBack.Content = "Cancelar";

            try
            {
                if (BioEngineFactory.getBio().OnCaptureOk==null)
                    BioEngineFactory.getBio().OnCaptureOk += onFPCapture;
                BioEngineFactory.getBio().startReading();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                getImgOtro();
                return;
            }

        }

        public void showFPImage()
        {
            fpFromRdr.setHuella(currDNI.WpfImgHuellaSensor, currDNI.QualityFromSensor);
        }

        public delegate void voidProc();
        public voidProc doShowImage;

        private void onFPCapture(Fmd fmd,Bitmap bmp)
        {
            BioEngineFactory.getBio().resetReaders();
            currDNI.loadFromSensor(fmd, bmp);
            doShowImage = showFPImage;
            Dispatcher.Invoke(doShowImage);
            bool OK = false;
            if (currDNI.Matches())
            {
                OK = true;
                MessageBox.Show("Las huellas coinciden","Comparación de huellas", MessageBoxButton.OK, MessageBoxImage.Information);
                currDNI.HuellaOk = true;
            } else
            {
                MessageBoxResult r = MessageBox.Show("Las huellas no coinciden. ¿Desea aceptarlas igualmente?", "Comparación de huellas", MessageBoxButton.YesNo, MessageBoxImage.Error);
                OK = (r == MessageBoxResult.Yes);
            }

            if (OK)
            {
                Dispatcher.Invoke((voidProc)saveToDB);
            }
            else
                Dispatcher.Invoke((voidProc)getFP);
        }

        public void cancelFP()
        {
            BioEngineFactory.getBio().resetReaders();
            getFP();
        }


        public void loadImage()
        {
            try
            {
                OpenFileDialog opf = new OpenFileDialog();

                if (opf.ShowDialog() == true)
                {
                    BitmapImage src = new BitmapImage();
                    src.BeginInit();
                    src.UriSource = new Uri(opf.FileName);
                    src.CacheOption = BitmapCacheOption.OnLoad;
                    src.EndInit();
                    imageDest.Source = src;
                    if (imageDest == imgBarCode)
                        decodeImg();
                }
                else
                {
                    throw new Exception("Finalizada por el usuario");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (imageDest == imgBarCode)
                getImgOtro();
            else
                getFP();
        }

        public void getImgOtro()
        {
            Logger.startScan();
            taskArea.CurrTaskID = "imgScan";
            taskArea.TaskText = "Dele la vuelta al DNI";
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnBack.Visibility = Visibility.Visible;
            taskArea.BackFunc = getBarCodeImage;
            taskArea.Busy.Visibility = Visibility.Hidden;

            imageDest = imgOtro;

            if ((devicePresent || Properties.Settings.Default.IsInProd) && !Properties.Settings.Default.testDNI)
            {
                taskArea.GoFunc = scanImage;
            }
            else
            {
                taskArea.GoFunc = loadImage;
            }
        }

        protected int lastDBID = -1;

        private void saveToDB()
        {
            taskArea.CurrTaskID = "saveImgToDB";
            taskArea.TaskText = "Grabando datos en la base";
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.Busy.Visibility = Visibility.Visible;
            try
            {
                currDNI.saveToDB();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                MessageBox.Show("Problemas con la grabación en la base de datos. Imposible continuar", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            taskArea.TaskText = "Proceso finalizado";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.Busy.Visibility = Visibility.Collapsed;
            taskArea.GoFunc = endWork;

        }

        public DNI.DNI_Bio currDNI = null;

        public void decodeImg()
        {

            currDNI = Decoder.decodeImg((BitmapSource)imageDest.Source);
            if (currDNI == null)
                throw new Exception("No se puede procesar el código de barras en la imagen");
            currDNI.loadBMPScanner(Decoder.ToWinFormsBitmap((BitmapSource)imageDest.Source), Properties.Settings.Default.Dpi, true);
            if (currDNI.HuellaEnAnverso)
            {
                currDNI.setMinutiaeScanner();
                fpFromScanner.setHuella(currDNI.WpfImgHuellaScanner,currDNI.QualityFromScanner);
            }
            gridData.Visibility = Visibility.Collapsed;
            dniNumero.Text = currDNI.Numero;
            dniApellidos.Text = currDNI.Apellidos;
            dniNombres.Text = currDNI.Nombres;
            dniSexo.Text = currDNI.Sexo;
            dniNacionalidad.Text = currDNI.Nacionalidad;
            dniFechaNacimiento.Text = currDNI.FechaNacimiento;
            gridData.Visibility = Visibility.Visible;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //e.Cancel = true;
            //endWork();
        }

        public void startWork()
        {
            try
            {
                
                //imgBarCode.Source = Decoder.GetImageStream(Properties.Resources.im_invisible_user);
                //imgOtro.Source = Decoder.GetImageStream(Properties.Resources.im_invisible_user);
                dniNumero.Text =
                dniApellidos.Text =
                dniNombres.Text =
                dniSexo.Text =
                dniNacionalidad.Text =
                dniFechaNacimiento.Text = "";
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                if (Properties.Settings.Default.IsInProd)
                {
                    MessageBox.Show("Imposible conectarse con el scanner", "error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            getBarCodeImage();
        }


        public void endWork()
        {
            Visibility = Visibility.Hidden;

            Application.Current.Shutdown();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            endWork();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Logger.startApp();

            startWork();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            taskArea.Busy.Visibility = Visibility.Visible;
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.txtTask.Content = "Un momento, por favor ...";
        }

        // Todo lo relativo al scanner
        DataSource scanner = null;
        private void initScanner()
        {
            var appId = TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly());

            TwainSession session = new TwainSession(appId);

            session.TransferReady += transferReady;
            session.DataTransferred += dataTransferred;
            session.TransferError += transferError;

            session.Open();

            string devStr = Properties.Settings.Default.devStr;
            foreach (DataSource ds in session)
            {
                if (ds.Name.Contains(devStr))
                {
                    scanner = ds;
                    scanner.Open();
                    break;
                }
            }

            devicePresent = scanner != null && scanner.IsOpen;
            if (devicePresent)
            {
                try
                {                    
                    scanner.Capabilities.ICapPixelType.SetValue(PixelType.RGB);
                    scanner.Capabilities.ICapAutomaticRotate.SetValue(Properties.Settings.Default.AutomaticRotate?BoolType.True:BoolType.False);
                    AutoSize doAutoSize;
                    if (Properties.Settings.Default.AutomaticBorderDetection)
                        doAutoSize = AutoSize.Auto;
                    else
                        doAutoSize = AutoSize.None;
                    scanner.Capabilities.ICapAutoSize.SetValue(doAutoSize);
                    scanner.Capabilities.ICapXResolution.SetValue((float)Properties.Settings.Default.Dpi);
                    scanner.Capabilities.ICapYResolution.SetValue((float)Properties.Settings.Default.Dpi);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }

        }

        private void startScan()
        {
            /*
            if (scanner.IsOpen)
                scanner.Close();

            scanner.Open();
            */
            SourceEnableMode sem;
            if (Properties.Settings.Default.ShowTwainUI)
                sem = SourceEnableMode.ShowUI;
            else
                sem = SourceEnableMode.NoUI;
            scanner.Enable(sem, false, (IntPtr)0);

        }

        private void transferError(object sender, TransferErrorEventArgs e)
        {
            scanOK = false;


            /*if (e.Exception.ReturnCode == TwRC.Cancel)
            {
                scanTxt = "Operación cancelada por el usuario";
                Logger.Info(scanTxt);
            }
            else if (e.Exception.ReturnCode == TwRC.Success)
            {
                scanTxt = e.Exception.Message;
                Logger.Error(scanTxt);
            }
            else
            {
                scanTxt = "Error en el acceso al scanner";
                Logger.Error(scanTxt);
                Logger.Error(e.Exception.Message);
            }
            */

        }

        private delegate void bmpProc(System.Drawing.Image img);
        private bmpProc doProcessImage;

        private void processImage(System.Drawing.Image img)
        {

            taskArea.Busy.Visibility = Visibility.Hidden;

            imageDest.Source = Decoder.GetImageStream(img);
            scanOK = true;
            if (imageDest == imgBarCode)
            {
                try
                {
                    decodeImg();
                    scanOK = true;
                }
                catch (Exception ex)
                {
                    scanTxt = ex.Message;
                    scanOK = false;
                }
            }
            else
            {
                currDNI.loadBMPScanner(Decoder.ToWinFormsBitmap((BitmapSource)imageDest.Source), Properties.Settings.Default.Dpi, false);
                if (!currDNI.HuellaEnAnverso)
                {
                    currDNI.setMinutiaeScanner();
                    fpFromScanner.setHuella(currDNI.WpfImgHuellaScanner, currDNI.QualityFromScanner);
                }

            }

            if (scanOK)
            {
                // Paso al próximo
                if (imageDest == imgBarCode)
                    getImgOtro();
                else
                    getFP();
            }
            else
            {
                MessageBox.Show(scanTxt, "Error al leer la imagen", MessageBoxButton.OK, MessageBoxImage.Error);
                if (imageDest == imgBarCode)
                    getBarCodeImage();
                else
                    getImgOtro();
                return;
            }

        }

        private void dataTransferred(object sender, DataTransferredEventArgs e)
        {
            System.Drawing.Image img = null;
            if (e.NativeData != IntPtr.Zero)
            {
                var stream = e.GetNativeImageStream();
                if (stream != null)
                {
                    img = System.Drawing.Image.FromStream(stream);
                    doProcessImage = processImage;
                    Object[] param = new object[1];
                    param[0] = img;
                    Dispatcher.Invoke(doProcessImage, param);
                }
            }
        }

        private void transferReady(object sender, TransferReadyEventArgs e)
        {
            
        }


    }
}
