﻿namespace BDAPreven.DNI
{
	public class DNIFactory
	{
		public static DNI_Bio getNewInstance(string serialData)
		{
			if (serialData[0] == '@')
				return new DNIViejo(serialData);
			else
				return new DNINuevo(serialData);
		}
	}
}
