﻿using BDAPreven.BIO;
using DPUruNet;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BDAPreven.DNI
{
    public class Huella
    {
        public Image<Gray, byte> Img { get; private set; }
        public Fmd Tpl { get; private set; }
        public bool OK;

        public Huella(Image<Gray, byte> img, int dpi)
        {
            initData(img, dpi);
        }

        public Huella(Bitmap bmp, int dpi)
        {
            initData(new Image<Gray, byte>(bmp), dpi);
        }

        public Huella(Bitmap bmp, Fmd tpl)
        {
            initData(new Image<Gray, byte>(bmp), tpl);
        }

        private void initData(Image<Gray, byte> img, int dpi)
        {
            DataResult<Fmd> rc;
            rc = FeatureExtraction.CreateFmdFromRaw(ImgUtils.getRawData(img), 0, 0, img.Width, img.Height, dpi, Constants.Formats.Fmd.ISO);

            if (rc.ResultCode != Constants.ResultCode.DP_SUCCESS || rc.Data == null)
            {
                throw new Exception("Huella no utilizable");
            }
            initData(img, rc.Data);
        }

        private void initData(Image<Gray, byte> img, Fmd tpl)
        {
            Img = img;
            Tpl = tpl;
            OK = false;
        }


    }
}
