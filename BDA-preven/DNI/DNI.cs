﻿using BDAPreven.BIO;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace BDAPreven.DNI
{
    public class DNI
    {
        public string SerialData { get { return FSerialData; } }
        public string Numero { get { return getNumero(); } }
        public string Tipo { get { return getTipo(); } }
        public string Apellidos { get { return getApellidos(); } }
        public string Nombres { get { return getNombres(); } }
        public string Nacionalidad { get { return getNacionalidad(); } }
        public string FechaNacimiento { get { return getFechaNacimiento(); } }
        public string Sexo { get { return getSexoAsString(); } }

        protected string FSerialData;

        private string getSexoAsString()
        {
            string s = this.getSexo();
            if (s.Equals("F"))
                return "Femenino";
            if (s.Equals("M"))
                return "Masculino";
            return "Otros";
        }

        protected virtual string getNumero() { return ""; }
        protected virtual string getTipo() { return ""; }
        protected virtual string getApellidos() { return ""; }
        protected virtual string getNombres() { return ""; }
        protected virtual string getNacionalidad() { return ""; }
        protected virtual string getFechaNacimiento() { return ""; }
        public virtual string getFechaNacimientoAnsi()
        {
            string retVal = FechaNacimiento;
            if (retVal.Length < 10)
                return "";
            string a = retVal.Substring(6);
            string m = retVal.Substring(3, 2);
            string d = retVal.Substring(0, 2);
            return a + m + d;
        }
        public virtual string getSexo() { return ""; }

        protected string getDataByPos(int pos)
        {
            if (pos < arrData.Length)
                return arrData[pos].Trim();
            else
                return "";
        }

        private string[] arrData;

        public DNI(string serialData)
        {
            this.FSerialData = serialData;

            arrData = serialData.Split('@');
        }

    }
}
