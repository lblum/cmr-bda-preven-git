﻿namespace BDAPreven.DNI
{
	class DNIViejo : DNI_Bio
	{
		public DNIViejo(string serialData)
			: base(serialData)
		{
            HuellaEnAnverso = true;
            setRectHuella(5, 35, 60, 80);
        }

        protected override string getNumero() { return getDataByPos(1); }
		protected override string getTipo() { return getDataByPos(2); }
		protected override string getApellidos() { return getDataByPos(4); }
		protected override string getNombres() { return getDataByPos(5); }
		protected override string getNacionalidad() { return getDataByPos(6); }
		protected override string getFechaNacimiento() { return getDataByPos(7); }
		public override string getSexo() { return getDataByPos(8); }
	}
}
