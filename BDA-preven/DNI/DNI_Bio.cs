﻿using BDAPreven.BIO;
using BDAPreven.Singleton;
using DPUruNet;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using SourceAFIS.Templates;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace BDAPreven.DNI
{
    public class DNI_Bio : DNI
    {
        /*
         * Los datos biométricos
         */

        // Medidas en milímetros
        protected const int dniW = 86;   // Ancho del DNI
        protected const int dniH = 54;   // Alto del DNI

        // Resolución default de los lectores
        protected const int DPIDef = 500;      // Puntos / pulgada
        protected const float ppmm = 25.4F;    // mm por pulgada

        // Los datos del sensor
        /* UrU 4500 
        public int sensorW = 350;
        public int sensorH = 380;
        */

        /* Eikon
        */
        protected const int sensorW = 256;
        protected const int sensorH = 360;

        // Los bitmaps originales a ser grabados en la base de datos
        public Bitmap Anverso { get; protected set; } = null;  // Acá esta el código de barras
        public Bitmap Reverso { get; protected set; } = null;  // Este es el otro lado

        // La huella completa recolectada desde la imagen (en tonos de gris)
        protected Image<Gray, Byte> imgHuellaScanner = null;
        public Bitmap ImgHuellaScanner
        {
            get
            {
                if (imgHuellaScanner == null)
                    return null;
                return imgHuellaScanner.Bitmap;
            }
        }
        public BitmapSource WpfImgHuellaScanner
        {
            get
            {
                return BarCode.Decoder.GetImageStream(imgHuellaScanner.Bitmap);
            }
        }

        public DNI_Bio(string serialData) : base(serialData)
        {
        }


        // El rectángulo de la huella (en mm)
        protected Rectangle rectHuella = Rectangle.Empty;
        protected void setRectHuella(int t, int b, int l, int r)
        {
            rectHuella = new Rectangle(l, t, r - l, b - t);
        }

        // Indica donde se encuentra la huella
        public bool HuellaEnAnverso { get; protected set; } = true;

        // La resolución
        public int Resolucion { get; protected set; }

        // La carga de la imagen
        public void loadBMPScanner(string file, int resolucion, bool esAnverso)
        {
            loadBMPScanner(new Bitmap(file), resolucion, esAnverso);
        }

        public void loadBMPScanner(Bitmap bmp, int resolucion, bool esAnverso)
        {
            Resolucion = resolucion;
            if (esAnverso)
            {
                Anverso = bmp;
            } else
            {
                Reverso = bmp;
            }

            if (!(esAnverso ^ HuellaEnAnverso))
            {
                // Ajusto el rectángulo por la resolución
                float rr = resolucion / ppmm;
                Rectangle rect = new Rectangle();
                rect.X = (int)(rectHuella.X * rr);
                rect.Y = (int)(rectHuella.Y * rr);
                rect.Width = (int)(rectHuella.Width * rr);
                rect.Height = (int)(rectHuella.Height * rr);
                Image<Gray, byte> aux = new Image<Gray, byte>(bmp);
                float scale = (float)DPIDef / (float)resolucion;
                preprocesarHuella(aux.Copy(rect).Resize(scale, Inter.Linear));
            }
        }

        protected void preprocesarHuella(Image<Gray, byte> img)
        {
            imgHuellaScanner = img;//.Erode(2).Dilate(2);
            //imgHuella = img.Dilate(1).Erode(1);
        }

        // Las huellas reescaladas, movidas y deformadas
        List<Image<Gray, byte>> huellas = new List<Image<Gray, byte>>();
        private int maxDeltaZoom = Properties.Settings.Default.maxZoom;

        public void generateHuellasScanner()
        {
            Point cImg = getCentroidFPScanner();
            Point cTpl = getCentroidTplScanner();

            generateHuellasScanner2(cImg.X, cImg.Y);
            if (!cTpl.Equals(cImg))
                generateHuellasScanner2(cTpl.X, cTpl.Y);
        }

        public void generateHuellasScanner2(int c_x, int c_y)
        {
            /*
            int c_x = imgHuellaScanner.Width / 2;
            int c_y = imgHuellaScanner.Height / 2;
            */

            int X = c_x - sensorW / 2;
            int Y = c_y - sensorH / 2;
            int W = sensorW;
            int H = sensorH;
            if (X < 0)
                X = 0;
            if (Y < 0)
                Y = 0;
            if (X + W > imgHuellaScanner.Width)
                X = imgHuellaScanner.Width - W;
            if (Y + H > imgHuellaScanner.Height)
                Y = imgHuellaScanner.Width - H;

            float scaleX, scaleY;
            Image<Gray, byte> aux = null, aux1 = null, aux3 = null;
            //int maxMovePixeles = Y / 10;
            int maxMovePixeles = 1;
            int i, j, k;
            for (j = 0; j < maxMovePixeles; j++)
            {
                try
                {
                    aux3 = imgHuellaScanner.Copy(new Rectangle(X, Y + j * 10, W, H));
                    aux = new Image<Gray, byte>(aux3.Width, aux3.Height);
                    CvInvoke.FastNlMeansDenoising(aux3, aux);
                } catch (Exception ex)
                {
                    ;
                }
                huellas.Add(aux);
                float deltaScale = 0.05F;
                for (i = 0; i < maxDeltaZoom; i++)
                {
                    for (k = 0; k < maxDeltaZoom; k++)
                    {
                        scaleX = deltaScale * i;
                        scaleY = deltaScale * k;

                        if (i != 0 && k != 0)
                        {
                            int X1 = (int)((W * scaleX) / 2);
                            int Y1 = (int)((H * scaleY) / 2);
                            int W1 = (int)((W * (1 + scaleX)));
                            int H1 = (int)((H * (1 + scaleY)));

                            aux1 = aux.Resize(W1, H1, Inter.Linear).Copy(new Rectangle(X1, Y1, W, H));
                            huellas.Add(aux1);
                        }

                    }
                }
                if (j != 0)
                {
                    aux = imgHuellaScanner.Copy(new Rectangle(X, Y - j * 10, W, H));
                    huellas.Add(aux);
                    for (i = 0; i < maxDeltaZoom; i++)
                    {
                        for (k = 0; k < maxDeltaZoom; k++)
                        {
                            scaleX = deltaScale * i;
                            scaleY = deltaScale * k;

                            if (i != 0 && k != 0)
                            {
                                int X1 = (int)((W * scaleX) / 2);
                                int Y1 = (int)((H * scaleY) / 2);
                                int W1 = (int)((W * (1 + scaleX)));
                                int H1 = (int)((H * (1 + scaleY)));

                                aux1 = aux.Resize(W1, H1, Inter.Linear).Copy(new Rectangle(X1, Y1, W, H));
                                huellas.Add(aux1);
                            }

                        }
                    }
                }
            }
        }

        // Las huellas propiamente dichas
        protected List<Fmd> minutiae = new List<Fmd>();
        public int QualityFromScanner { get; private set; } = 0;
        public void setMinutiaeScanner()
        {
            if (huellas.Count == 0)
                generateHuellasScanner();
            foreach (Image<Gray, byte> huella in huellas)
            {
                DataResult<Fmd> resultConversion;
                resultConversion = FeatureExtraction.CreateFmdFromRaw(ImgUtils.getRawData(huella), 0, 0, huella.Width, huella.Height, DPIDef, Constants.Formats.Fmd.ISO);

                if (resultConversion.ResultCode == Constants.ResultCode.DP_SUCCESS && resultConversion.Data != null)
                {
                    minutiae.Add(resultConversion.Data);
                    if (resultConversion.Data.ViewCount > 0)
                    {
                        int q = resultConversion.Data.Views[0].Quality;
                        if (q > QualityFromScanner)
                            QualityFromScanner = q;
                    }
                }
            }
        }

        public void saveHuellasScanner(string path)
        {
            if (huellas.Count == 0)
                generateHuellasScanner();
            int i = 0;
            string fileName;
            foreach (Image<Gray, byte> huella in huellas)
            {
                fileName = string.Format(@"{0}\{1}-{2}.bmp", path, getNumero(), i++);
                huella.Save(fileName);
            }
        }

        #region Huella
        public List<Huella> Huellas = new List<Huella>();

        protected Huella LastHuella
        {
            get
            {
                return Huellas.Last();
            }
        }

        public Bitmap FpFomSensor
        {
            get
            {
                if (LastHuella == null)
                    return null;
                return LastHuella.Img.Bitmap;
            }
        }

        protected Image<Gray, byte> FimgFromSensor
        {
            get
            {
                if (LastHuella == null)
                    return null;
                return LastHuella.Img;
            }
        }

        public Fmd FTemplateFromSensor
        {
            get
            {
                if (LastHuella == null)
                    return null;
                return LastHuella.Tpl;
            }
        }

        public BitmapSource WpfImgHuellaSensor
        {
            get
            {
                return BarCode.Decoder.GetImageStream(FimgFromSensor.Bitmap);
            }
        }

        public int QualityFromSensor
        {
            get
            {
                if (FTemplateFromSensor == null || FTemplateFromSensor.ViewCount == 0)
                    return 0;
                return FTemplateFromSensor.Views[0].Quality;
            }
        }

        public bool HuellaOk
        {
            get
            {
                if (LastHuella == null)
                    return false;
                return LastHuella.OK;
            }
            set
            {
                if (LastHuella != null)
                    LastHuella.OK = value;
            }
        }

        public void loadFromSensor(Bitmap bmp)
        {
            loadFromSensor(bmp, DPIDef);
        }

        public void loadFromSensor(Bitmap bmp, int resolucion)
        {
            Huellas.Add(new Huella(bmp, resolucion));
        }

        public void loadFromSensor(Fmd fmd,Bitmap bmp)
        {
            Huellas.Add(new Huella(bmp,fmd));
        }

        private const int DPFJ_PROBABILITY_ONE = 0x7fffffff;
        private int thresholdScore = DPFJ_PROBABILITY_ONE * 1 / 1;

        public bool Matches()
        {
            IdentifyResult ir = Comparison.Identify(FTemplateFromSensor, 0, minutiae, thresholdScore, minutiae.Count);
            if (ir.Indexes.Count() > 0)
                return true;
            return false;
        }

        #endregion

        public void saveToDB()
        {
            if (Properties.Settings.Default.noDB)
                return;

            DBFactory.getNewInstance().saveDNI(this);

        }

        protected Point getCentroid(Image<Gray, Byte> img)
        {
            var moments = CvInvoke.Moments(img.Not().Mat, false);
            Point c = new Point((int)(moments.M10 / moments.M00), (int)(moments.M01 / moments.M00));
            return c;
        }

        public Point getCentroidFPScanner()
        {
            return getCentroid(imgHuellaScanner);
        }

        public Point getCentroidTplScanner()
        {
            Point res = getCentroidFPScanner();
            DataResult<Fmd> resultConversion;
            resultConversion = FeatureExtraction.CreateFmdFromRaw(ImgUtils.getRawData(imgHuellaScanner), 0, 0, imgHuellaScanner.Width, imgHuellaScanner.Height, DPIDef, Constants.Formats.Fmd.ISO);

            if (resultConversion.ResultCode == Constants.ResultCode.DP_SUCCESS && resultConversion.Data != null)
            {
                IsoFormat ifo = new IsoFormat(resultConversion.Data.Bytes);
                res = ifo.Centroid;
            }

            return res;

        }

    }
}
