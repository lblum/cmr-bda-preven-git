﻿namespace BDAPreven.DNI
{
	class DNINuevo : DNI_Bio
	{
		public DNINuevo(string serialData)
			: base(serialData)
		{
            HuellaEnAnverso = false;
            setRectHuella(5, 30, 65, 84);
        }

        protected override string getNumero() { return getDataByPos(4); }
		protected override string getTipo() { return getDataByPos(5); }
		protected override string getApellidos() { return getDataByPos(1); }
		protected override string getNombres() { return getDataByPos(2); }
		protected override string getFechaNacimiento() { return getDataByPos(6); }
		public override string getSexo() { return getDataByPos(3); }

	}
}
