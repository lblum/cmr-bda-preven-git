﻿using BDAPreven.BIO;
using BDAPreven.BIO.Eikon;
using BDAPreven.BIO.Uru;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDAPreven.Singleton
{
    public class BioEngineFactory
    {
        private static BioEngine FBio = null;

        public static BioEngine getBio()
        {
            if (FBio == null)
            {
                if (Properties.Settings.Default.fpDriver == "Uru")
                    FBio = null;// new BioEngineUru();
                else if (Properties.Settings.Default.fpDriver == "Eikon")
                    FBio = new BioEngineEikon();
            }

            return FBio;
        }


    }
}
