﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDAPreven.Singleton
{
    public class DBFactory
    {
        private static DB db = null;
        public static DB getNewInstance()
        {
            if (db == null)
            {
                if (Properties.Settings.Default.dbDriver == "mysql")
                    db = new MySQLDB();
                else
                    db = new SQLSDB();
            }

            return db;
        }
    }
}
