﻿using BDAPreven.DNI;
using System;
using System.Data;
using System.Data.Common;

namespace BDAPreven.Singleton
{
    public abstract class DB
    {
        private static object syncRoot = new Object();
        public DbConnection Conn { get; protected set; }

        public DB()
        {
            Conn = makeConn(Properties.Settings.Default.dbDSN);
            openConn();
        }

        protected abstract DbConnection makeConn(string dsn);

        public DbConnection openConn()
        {
            if (Conn.State != ConnectionState.Open)
                Conn.Open();
            return Conn;
        }

        public void closeConn()
        {
            if (Conn.State != ConnectionState.Closed)
                Conn.Close();
            if (Conn != null)
            {
                Conn = null;
            }
        }

        public abstract long saveDNIData(DNI_Bio dni);

        public void saveDNI(DNI_Bio dni)
        {
            long idDni = saveDNIData(dni);
            foreach (Huella fp in dni.Huellas)
            {
                saveFP(fp,idDni);
            }
        }

        public abstract DNI_Bio loadDNIFromDB(int id);

        protected abstract void saveFP(Huella fp, long idDni);

        public abstract DbDataReader loadDNIFromDB(string nro);
       

        public Object ToDbNull(Object o)
        {
            return o == null ? DBNull.Value : o;
        }

        public abstract DataView getDNIList(bool withImages);

        
    }
}
