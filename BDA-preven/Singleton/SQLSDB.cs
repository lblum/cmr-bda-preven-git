﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BDAPreven.DNI;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using BDAPreven.BIO;

namespace BDAPreven.Singleton
{
    public class SQLSDB : DB
    {
        public override long saveDNIData(DNI_Bio dni)
        {
            string dniStrSQL =
    "INSERT INTO [dbo].[dni]" +
    "           ([pdf_417_data]" +
    "           ,[dni]" +
    "           ,[apellidos]" +
    "           ,[nombres]" +
    "           ,[sexo]" +
    "           ,[nacionalidad]" +
    "           ,[fnacimiento]" +
    "           ,[img1]" +
    "           ,[img2]" +
    "           ,[fp_img])" +
    "    OUTPUT INSERTED.ID" +
    "    VALUES (@pdf_417_data" +
    "           ,@dni" +
    "           ,@apellidos" +
    "           ,@nombres" +
    "           ,@sexo" +
    "           ,@nacionalidad" +
    "           ,@fnacimiento" +
    "           ,@img1" +
    "           ,@img2" +
    "           ,@fp_img)";

            SqlCommand cmd = (SqlCommand)Conn.CreateCommand();
            cmd.CommandText = dniStrSQL;
            cmd.Parameters.Add("@pdf_417_data", SqlDbType.VarChar).Value = ToDbNull(dni.SerialData);
            cmd.Parameters.Add("@dni", SqlDbType.VarChar).Value = ToDbNull(dni.Numero);
            cmd.Parameters.Add("@apellidos", SqlDbType.VarChar).Value = ToDbNull(dni.Apellidos);
            cmd.Parameters.Add("@nombres", SqlDbType.VarChar).Value = ToDbNull(dni.Nombres);
            cmd.Parameters.Add("@sexo", SqlDbType.VarChar).Value = ToDbNull(dni.Sexo);
            cmd.Parameters.Add("@nacionalidad", SqlDbType.VarChar).Value = ToDbNull(dni.Nacionalidad);
            DateTime dt = DateTime.ParseExact(dni.FechaNacimiento, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string strDate = dt.ToString("yyyyMMdd");
            cmd.Parameters.Add("@fnacimiento", SqlDbType.VarChar).Value = ToDbNull(strDate);
            cmd.Parameters.Add("@img1", SqlDbType.VarBinary).Value = ToDbNull(ImgUtils.imageToByteArrayFmt(dni.Anverso));
            cmd.Parameters.Add("@img2", SqlDbType.VarBinary).Value = ToDbNull(ImgUtils.imageToByteArrayFmt(dni.Reverso));
            cmd.Parameters.Add("@fp_img", SqlDbType.VarBinary).Value = ToDbNull(ImgUtils.imageToByteArray(dni.ImgHuellaScanner));
            long lastId = Convert.ToInt32(cmd.ExecuteScalar());

            return lastId;
        }

        public override DbDataReader loadDNIFromDB(string nro)
        {
            string sqlStr = "select * from [dbo].[imgs] where [dni]=dni";
            SqlCommand cmd = (SqlCommand)Conn.CreateCommand();
            cmd.CommandText = sqlStr;
            return cmd.ExecuteReader();
        }


        protected override DbConnection makeConn(string dsn)
        {
            SqlConnection conn = new SqlConnection(dsn);
            return conn;
        }

        protected override void saveFP(Huella fp,long idDni)
        {
            string fpStrSQL =
    "INSERT INTO fp" +
    "           (id_dni" +
    "           ,img" +
    "           ,tpl" +
    "           ,ok)" +
    "    VALUES (@id_dni" +
    "           ,@img" +
    "           ,@tpl" +
    "           ,@ok)";
            SqlCommand cmd = (SqlCommand)Conn.CreateCommand();
            cmd.CommandText = fpStrSQL;
            cmd.Parameters.Add("@id_dni", SqlDbType.Int).Value = idDni;
            cmd.Parameters.Add("@img", SqlDbType.VarBinary).Value = ToDbNull(ImgUtils.imageToByteArray(fp.Img.Bitmap));
            cmd.Parameters.Add("@tpl", SqlDbType.VarBinary).Value = ToDbNull(fp.Tpl.Bytes);
            cmd.Parameters.Add("@ok", SqlDbType.Int).Value = fp.OK ? 1 : 0;
            cmd.ExecuteNonQuery();
        }

        public override DataView getDNIList(bool withImages)
        {
            string strSQL =
    "select [d].[id]" +
    "      ,[d].[ts]" +
    "      ,[d].[pdf_417_data]" +
    "      ,[d].[dni]" +
    "      ,[d].[apellidos]" +
    "      ,[d].[nombres]" +
    "      ,[d].[sexo]" +
    "      ,[d].[nacionalidad]" +
    "      ,[d].[fnacimiento]"+
    "      ,coalesce(ffp.n,0) as ok";
            if (withImages)
            {
                strSQL += ",[d].[img1],[d].[img2],[d].[fp_img] ";
            }
            strSQL += " from dni d ";
            strSQL += " left outer join ( select fp.id_dni as fp_id,count(*) as n from fp where ok=1 group by fp.id_dni) as ffp on ffp.fp_id = d.id ";
            /*if (withImages)
                strSQL += " left outer join [fp] on [fp].[id_dni]=[d].[id] ";*/

            strSQL += " order by [d].[id] desc";
            SqlCommand cmd = (SqlCommand)DBFactory.getNewInstance().Conn.CreateCommand();
            cmd.CommandText = strSQL;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();
            cmd.Dispose();
            return ds.Tables[0].DefaultView;
        }

        public override DNI_Bio loadDNIFromDB(int id)
        {
            string strSQL =
            "select [pdf_417_data]" +
            "      ,[img1]" +
            "      ,[img2]" +
            "      ,[fp_img]" +
            " from [dni] " +
            "where [id]= @id";
            DNI_Bio retVal = null;
            using (SqlCommand cmd = (SqlCommand)DBFactory.getNewInstance().Conn.CreateCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        return null;
                    retVal = DNIFactory.getNewInstance(dr.GetString(0));
                    //retVal.loadBMPScanner()
                }
            }

            return retVal;


        }
    }
}
