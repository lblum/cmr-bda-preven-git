﻿using System;
using System.Data;
using System.Data.Odbc;

namespace BDAPreven.Singleton
{
    public class DB_old
    {
        private static object syncRoot = new Object();
        private static OdbcConnection dbConn = null;
        private static OdbcConnection duplConn = null;

        private DB_old()
        {
        }

        private static OdbcConnection getConn()
        {
            if (dbConn == null)
            {
                lock (syncRoot)
                {
                    if (dbConn == null)
                    {
                        dbConn = new OdbcConnection(Properties.Settings.Default.dbDSN);
                    }
                }
            }
            return dbConn;
        }

        public static OdbcConnection openConn()
        {
            if (getConn().State != ConnectionState.Open)
                getConn().Open();
            return getConn();
        }

        public static OdbcConnection duplicateConn()
        {
            if (duplConn == null)
            {
                lock (syncRoot)
                {
                    if (duplConn == null)
                    {
                        duplConn = new OdbcConnection(Properties.Settings.Default.dbDSN);
                    }
                }
            }
            return duplConn;
        }

        public static void closeConn()
        {
            if (getConn().State != ConnectionState.Closed)
                getConn().Close();
            if (dbConn != null)
            {
                lock (syncRoot)
                {
                    if (dbConn != null)
                    {
                        dbConn = null;
                    }
                }
            }
        }

        public static void closeDuplConn()
        {
            if (duplConn.State != ConnectionState.Closed)
                duplConn.Close();
            if (duplConn != null)
            {
                lock (syncRoot)
                {
                    if (duplConn != null)
                    {
                        duplConn = null;
                    }
                }
            }
        }

        private static string getParam(string paramName, string paramDefault)
        {
            string retVal = paramDefault;
            OdbcCommand cmd = null;
            OdbcDataReader rdr = null; ;
            try
            {
                cmd = openConn().CreateCommand();
                cmd.CommandText = "select paramVal from param where paramName=?";
                cmd.Parameters.Add("paramName", OdbcType.VarChar).Value = paramName;
                rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    retVal = rdr.GetString(0);
                }
            } catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            finally
            {
                rdr.Close();
            }
            return retVal;
        }

        public static float maxImporte()
        {
            float retVal = float.Parse(getParam("maxImp", "0"));
            return retVal;
        }
    }
}
