﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BDAPreven.DNI;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Globalization;
using BDAPreven.BIO;
using System.Data;

namespace BDAPreven.Singleton
{
    class MySQLDB : DB
    {
        public override DataView getDNIList(bool withImages)
        {
            throw new NotImplementedException();
        }

        public override DNI_Bio loadDNIFromDB(int id)
        {
            throw new NotImplementedException();
        }

        public override DbDataReader loadDNIFromDB(string nro)
        {
            throw new NotImplementedException();
        }

        public override long saveDNIData(DNI_Bio dni)
        {
            string dniStrSQL =
    "INSERT INTO dni" +
    "           (pdf_417_data" +
    "           ,dni" +
    "           ,apellidos" +
    "           ,nombres" +
    "           ,sexo" +
    "           ,nacionalidad" +
    "           ,fnacimiento" +
    "           ,img1" +
    "           ,img2" +
    "           ,fp_img)" +
    "    VALUES (@pdf_417_data" +
    "           ,@dni" +
    "           ,@apellidos" +
    "           ,@nombres" +
    "           ,@sexo" +
    "           ,@nacionalidad" +
    "           ,@fnacimiento" +
    "           ,@img1" +
    "           ,@img2" +
    "           ,@fp_img)";
            MySqlCommand cmd = (MySqlCommand)Conn.CreateCommand();
            cmd.CommandText = dniStrSQL;
            cmd.Parameters.Add("@pdf_417_data", MySqlDbType.Text).Value = ToDbNull(dni.SerialData);
            cmd.Parameters.Add("@dni", MySqlDbType.VarChar).Value = ToDbNull(dni.Numero);
            cmd.Parameters.Add("@apellidos", MySqlDbType.Text).Value = ToDbNull(dni.Apellidos);
            cmd.Parameters.Add("@nombres", MySqlDbType.Text).Value = ToDbNull(dni.Nombres);
            cmd.Parameters.Add("@sexo", MySqlDbType.Text).Value = ToDbNull(dni.Sexo);
            cmd.Parameters.Add("@nacionalidad", MySqlDbType.Text).Value = ToDbNull(dni.Nacionalidad);
            DateTime dt = DateTime.ParseExact(dni.FechaNacimiento, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string strDate = dt.ToString("yyyy-MM-dd");
            cmd.Parameters.Add("@fnacimiento", MySqlDbType.Text).Value = ToDbNull(strDate);
            cmd.Parameters.Add("@img1", MySqlDbType.MediumBlob).Value = ToDbNull(ImgUtils.imageToByteArrayFmt(dni.Anverso));
            cmd.Parameters.Add("@img2", MySqlDbType.MediumBlob).Value = ToDbNull(ImgUtils.imageToByteArrayFmt(dni.Reverso));
            cmd.Parameters.Add("@fp_img", MySqlDbType.MediumBlob).Value = ToDbNull(ImgUtils.imageToByteArray(dni.ImgHuellaScanner));
            cmd.ExecuteNonQuery();
            long lastId = cmd.LastInsertedId;

            return lastId;
        }

        protected override DbConnection makeConn(string dsn)
        {
            MySqlConnection conn = new MySqlConnection(dsn);
            return conn;
        }

        protected override void saveFP(Huella fp, long id_dni)
        {
            string fpStrSQL =
    "INSERT INTO fp" +
    "           (id_dni" +
    "           ,img" +
    "           ,tpl" +
    "           ,ok)" +
    "    VALUES (@id_dni" +
    "           ,@img" +
    "           ,@tpl" +
    "           ,@ok)";
            MySqlCommand cmd = (MySqlCommand)Conn.CreateCommand();
            cmd.CommandText = fpStrSQL;
            cmd.Parameters.Add("@id_dni", MySqlDbType.Int16).Value = id_dni;
            cmd.Parameters.Add("@img", MySqlDbType.MediumBlob).Value = ToDbNull(ImgUtils.imageToByteArray(fp.Img.Bitmap));
            cmd.Parameters.Add("@tpl", MySqlDbType.MediumBlob).Value = ToDbNull(fp.Tpl.Bytes);
            cmd.Parameters.Add("@ok", MySqlDbType.Int16).Value = fp.OK ? 1 : 0;
            cmd.ExecuteNonQuery();
        }
    }
}
