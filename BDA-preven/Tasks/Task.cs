﻿using System;

namespace BDAPreven.Tasks
{
    public abstract class Task
    {
        protected MainWindow owner;
        protected WPFControls.Task taskCtl;

        public Task(MainWindow p_owner)
        {
            owner = p_owner;
            taskCtl = owner.taskArea;
        }

        private string FCurrTaskId = "";
        public string CurrTaskId
        {
            get
            {
                return FCurrTaskId;
            }
            set
            {
                setCurrTaskId(value);
            }
        }

        public abstract void prologo();
        public abstract void epilogo();
        public abstract bool goFunc();
        public abstract bool backFunc();

        private void setCurrTaskId(string value)
        {
            FCurrTaskId = value;
            taskCtl.CurrTaskID = value;
        }


    }
}
