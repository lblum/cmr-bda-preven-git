﻿using BDAPreven.Singleton;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BDAPreven.BIO;
using DPUruNet;
using System.Drawing;

namespace BDAPreven
{
    /// <summary>
    /// Lógica de interacción para TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        public TestWindow()
        {
            InitializeComponent();
            BioEngineFactory.getBio().OnCaptureOk = onFPCapture;
        }

        // Viejo
        string dniData = @"@1196445@A@2@ALMEIDA@ALCIRA MAGDALENA@ARGENTINA@01/01/2000@F@03/02/2012@00093073501@7108 @27/06/2021@555@0@ILR:2.30 C:120205.03@UNIDAD #03  || S/N: 0040>2008>>0017";
        // Nuevo
        //string dniData = @"00198801222@MIRANDA@LAUTARO@M@53243277@A@16/05/2013@14/06/2013";

        DNI.DNI_Bio test;

        private void button_Click(object sender, RoutedEventArgs e)
        {

            Stopwatch sp = new Stopwatch();
            sp.Start();
            //doTest(@"c:\tmp\bmp\fromDNIFujitsu_500_dpi.bmp", 500, @"c:\tmp\bmp\1.bmp");
            doTest(@"c:\tmp\bmp\lb\lb-300dpi.bmp", 300, @"c:\tmp\bmp\lb\1.bmp");
            //doTest(@"c:\tmp\bmp\dni_from_scanner1.bmp", 500, @"c:\tmp\bmp\1.bmp");
            //doTest(@"C:\tmp\bmp\mmr\mmr-300dpi.bmp", 300, @"C:\tmp\bmp\mmr\1.bmp");
            sp.Stop();

        }

        public void showFPImage(BitmapSource src)
        {
            sensor.Source = src;
        }

        public delegate void ShowImage(BitmapSource src);
        public ShowImage doShowImage;

        private void onFPCapture(Fmd fmd,Bitmap bmp)
        {

            test.loadFromSensor(fmd, bmp);
            doShowImage = showFPImage;
            Dispatcher.Invoke(doShowImage, test.WpfImgHuellaSensor);
            //sensor.Source = test.WpfImgHuellaSensor;

            if (test.Matches())
                Console.WriteLine("Si");
            else
                Console.WriteLine("No");
        }

        private void doTest(string dni,int resolucion,string fp)
        {
            test = DNI.DNIFactory.getNewInstance(dniData);
            test.loadBMPScanner(dni, resolucion, test.HuellaEnAnverso);
            huella.Source = test.WpfImgHuellaScanner;
            test.generateHuellasScanner();
            test.saveHuellasScanner(@"c:\tmp\bmp\mmr\opencv");
            
            test.setMinutiaeScanner();
            System.Drawing.Point p = test.getCentroidFPScanner();
            
            int h = (int)(vertL.ActualHeight / 2);
            int w = (int)(horiL.ActualWidth / 2);
            vertL.X1 = vertL.X2 = p.X;
            vertL.Y1 = p.Y - h;
            vertL.Y2 = p.Y + h;
            horiL.Y1 = horiL.Y2 = p.Y;
            horiL.X1 = p.X - w;
            horiL.X2 = p.X + w;
            horiL.Visibility = Visibility.Visible;
            vertL.Visibility = Visibility.Visible;

            vertL1.X1 = vertL1.X2 = huella.ActualWidth/2;
            vertL1.Y1 = (huella.ActualHeight / 2) - h;
            vertL1.Y2 = (huella.ActualHeight / 2) + h;
            horiL1.Y1 = horiL1.Y2 = (huella.ActualHeight / 2);
            horiL1.X1 = (huella.ActualWidth / 2) - w;
            horiL1.X2 = (huella.ActualWidth / 2) + w;
            horiL1.Visibility = Visibility.Visible;
            vertL1.Visibility = Visibility.Visible;
            /*
            test.FpFromSensor = new System.Drawing.Bitmap(fp);
            if (test.Matches())
                Console.WriteLine("Si");
            else
                Console.WriteLine("No");
                */
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            BioEngineFactory.getBio().startReading();

        }
    }
}
