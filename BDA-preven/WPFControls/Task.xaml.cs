﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace BDAPreven.WPFControls
{
    /// <summary>
    /// Interaction logic for Task.xaml
    /// </summary>
    public partial class Task : UserControl
    {
        public Task()
        {
            InitializeComponent();
            currTaskID = null;
        }

        private string currTaskID = null;
        public string CurrTaskID {
            get
            {
                return currTaskID;
            }
            set
            {
                Window parent = Window.GetWindow(this);
                if (CurrTaskID != null)
                {
                    Border currBorder = (Border)parent.FindName(currTaskID);
                    currBorder.Style = (Style)FindResource("roundBorderStyle"); ;
                }

                Border newTask = (Border)parent.FindName(value);
                Style glowStyle = (Style)FindResource("workingEf");
                newTask.Style = glowStyle;

                currTaskID = value;

            }
        }

        public Button BntGo
        {
            get
            {
                return btnGo;
            }
        }

        public Button BntBack
        {
            get
            {
                return btnBack;
            }
        }

        public string TaskText
        {
            get
            {
                return txtTask.Content.ToString();
            }
            set
            {
                txtTask.Content = value;
            }
        }

        public delegate void GoFuncDel();
        public GoFuncDel GoFunc;
        public delegate void BackFuncDel();
        public BackFuncDel BackFunc;

        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GoFunc();
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BackFunc();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
