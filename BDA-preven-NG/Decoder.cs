﻿using System.IO;
using System.Windows.Media.Imaging;
using System.Drawing;
using System;
using System.Windows;
using ZXing;

namespace BDAPreven.BarCode
{
    public class Decoder
    {
        private readonly static BarcodeReader reader = new BarcodeReader();

        private readonly static Rectangle[] bcArea = {
            new Rectangle(50, 40, 45, 14),
            new Rectangle(40, 35, 45, 20)
        };

        protected const float ppmm = 25.4F;    // mm por pulgada

        protected static Bitmap[] getBmpArea(Bitmap bmp,int dpi)
        {
            Bitmap[] retVal = new Bitmap[2];

            float r = dpi / ppmm;

            for ( int i = 0; i<2; i++)
            {
                Rectangle a = new Rectangle((int)(bcArea[i].X * r),
                                            (int)(bcArea[i].Y * r),
                                            (int)(bcArea[i].Width * r),
                                            (int)(bcArea[i].Height * r));
                Bitmap bmpD = new Bitmap(a.Width, a.Height);
                Graphics g = Graphics.FromImage(bmpD);
                g.DrawImage(bmp,0,0,a, GraphicsUnit.Pixel);
                retVal[i] = bmpD;
            }

            return retVal;
        }

        public static DNI.DNI decodeImg(BitmapSource src, int dpi)
        {
            Result result;
            Bitmap bmp = ImgUtils.ToWinFormsBitmap(src);

            Bitmap[] b = getBmpArea(bmp, dpi);
            result = reader.Decode(b[0]);
            if (result == null)
            {
                result = reader.Decode(b[1]);

                if (result == null)
                    throw new Exception("Problemas al leer el código de barras");
            }

            DNI.DNI retVal = DNI.DNIFactory.getNewInstance(result.Text);           

            return retVal;

        }

    }
}
