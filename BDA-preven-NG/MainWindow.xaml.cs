﻿using System;
using System.Windows;
using BDAPreven.Singleton;
using BDAPreven.BarCode;
using System.Windows.Media.Imaging;
using DPUruNet;
using System.Drawing;
using Tesseract;
using BDAPreven.Properties;
using System.Windows.Media;

namespace BDAPreven
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        protected bool mustLoadFP = true;
        public MainWindow()
        {
            InitializeComponent();

            int i;

            // Parámetros de imágenes
            DNI.DNI.MaxDeltaZoom = Properties.Settings.Default.MaxDeltaZoom;
            ImgUtils.ImgFmt = Properties.Settings.Default.ImgFmt;
            ImgUtils.ImgQ = Properties.Settings.Default.ImgQ;

            GlobalConfig.dbDriver = Properties.Settings.Default.dbDriver;
            GlobalConfig.dbDSN = Properties.Settings.Default.dbDSN;
            mustLoadFP = Properties.Settings.Default.fp;
            DBFactory.getDB();
            panelFP.Visibility = mustLoadFP ? Visibility.Visible : Visibility.Hidden;
            panelOCR.Visibility = !mustLoadFP ? Visibility.Visible : Visibility.Hidden;
        }

        private Scanner scanner;
        private System.Windows.Controls.Image imageDest;
        private DNI.DNI currDNI;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            taskArea.GoBtn.Visibility = Visibility.Collapsed;
            taskArea.BackBtn.Visibility = Visibility.Collapsed;
            taskArea.BusyInd.Visibility = Visibility.Visible;

            taskArea.TaskText = "Un momento, por favor ...";
            Logger.Info("Arranque de la aplicación");
            try
            {
                scanner = new Scanner();
                scanner.OkHandler = OnScanOk;
                scanner.ErrHandler = OnScanErr;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            if (!scanner.DevicePresent)
            {
                taskArea.Visibility = Visibility.Collapsed;
                WpfMessageBox.Show("Error en el scanner", "Imposible la comunicación con el scanner", MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);
            }
            else
            {
                getBarCodeImage();
            }
        }

        #region Scanner
        private void OnScanErr()
        {
            WpfMessageBox.Show("Error en el escaneo", scanner.LastException, MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);
            Exception lastEx = null;
            Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    if (imageDest == imgBarCode)
                        getBarCodeImage();
                    else
                        getImgOtro();
                }
                catch (Exception ex)
                {
                    lastEx = ex;
                }
            }));
            if (lastEx != null)
                WpfMessageBox.Show("Error en el escaneo", lastEx.Message, MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);
        }

        private void OnScanOk()
        {
            Exception lastExcepcion = null;
            Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    imageDest.Source = ImgUtils.GetImageStream(scanner.LastImage);
                    bool isAnverso = imageDest == imgBarCode;
                    if (isAnverso)
                    {
                        currDNI = Decoder.decodeImg((BitmapSource)imageDest.Source, Properties.Settings.Default.Dpi);
                        if (currDNI.Numero == null || currDNI.Numero == "")
                            throw new Exception("Problemas con la lectura del código de barras");
                        currDNI.setMaxThr(Properties.Settings.Default.maxThr);
                        dniNumero.Text = currDNI.Numero;
                        dniApellidos.Text = currDNI.Apellidos;
                        dniNombres.Text = currDNI.Nombres;
                        dniSexo.Text = currDNI.getSexo();
                        dniNacionalidad.Text = currDNI.Nacionalidad;
                        dniFechaNacimiento.Text = currDNI.FechaNacimiento;
                        dniEjemplar.Text = currDNI.Ejemplar;
                    }
                    currDNI.loadBMPScanner(ImgUtils.ToWinFormsBitmap((BitmapSource)imageDest.Source), Properties.Settings.Default.Dpi, imageDest == imgBarCode);
                    if (isAnverso ^ !currDNI.HuellaEnAnverso)
                    {
                        if (mustLoadFP)
                        {
                            currDNI.generateHuellas();
                            currDNI.setMinutiaeScanner();
                            fpFromScanner.setHuella(currDNI.WpfImgHuellaScanner, currDNI.QualityFromScanner);
                        }
                    }

                    if (isAnverso ^ !currDNI.FirmaEnAnverso)
                    {
                        Bitmap firma = currDNI.Firma;
                        imgFirma.Source = ImgUtils.GetImageStream(firma);
                    }

                    if (isAnverso)
                        getImgOtro();
                    else
                    {
                        if (mustLoadFP)
                            getFP();
                        else
                            getOCR();
                    }

                }
                catch (Exception ex)
                {
                    lastExcepcion = ex;
                }
            }));
            if (lastExcepcion != null)
                throw lastExcepcion;
        }

        private void getBarCodeImage()
        {
            Logger.startBarCode();
            dniNumero.Text =
            dniApellidos.Text =
            dniNombres.Text =
            dniSexo.Text =
            dniNacionalidad.Text =
            dniEjemplar.Text =
            dniFechaNacimiento.Text = "";
            taskArea.CurrTaskID = "barCodeScan";
            taskArea.TaskText = "Coloque el DNI en el scanner con el código de barras hacia abajo";
            taskArea.BusyInd.Visibility = Visibility.Hidden;
            taskArea.GoBtn.Content = "OK";
            taskArea.GoBtn.Visibility = Visibility.Visible;
            taskArea.BackBtn.Visibility = Visibility.Collapsed;
            imageDest = imgBarCode;
            taskArea.GoFunc = scanImage;
        }

        private void getImgOtro()
        {
            Logger.startScan();
            taskArea.CurrTaskID = "imgScan";
            taskArea.TaskText = "Dele la vuelta al DNI";
            taskArea.GoBtn.Content = "OK";
            taskArea.GoBtn.Visibility = Visibility.Visible;
            taskArea.BackBtn.Visibility = Visibility.Visible;
            taskArea.BackFunc = getBarCodeImage;
            taskArea.BusyInd.Visibility = Visibility.Hidden;
            imageDest = imgOtro;
            taskArea.GoFunc = scanImage;
        }

        private void scanImage()
        {
            taskArea.TaskText = "Escaneando ...";
            taskArea.GoBtn.Visibility = Visibility.Collapsed;
            taskArea.BackBtn.Visibility = Visibility.Collapsed;
            taskArea.BusyInd.Visibility = Visibility.Visible;
            try
            {
                scanner.startScan();
            }
            catch (Exception ex)
            {
                WpfMessageBox.Show(ex.GetType().Name, ex.Message, MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);

                if (imageDest == imgBarCode)
                    getBarCodeImage();
                else
                    getImgOtro();
                return;
            }
        }
        #endregion
        private void getOCR()
        {
            Bitmap bmp = currDNI.OCR;
            imgOCR.Source = ImgUtils.GetImageStream(bmp);
            var engine = new TesseractEngine(@"./tessdata", "eng");
            engine.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ<>1234567890");
            engine.SetVariable("load_system_dawg", false);
            engine.SetVariable("load_freq_dawg", false);
            engine.SetVariable("tessedit_enable_bigram_correction", false);
            engine.SetVariable("load_bigram_dawg", false);
            engine.SetVariable("save_alt_choices", false);
            bool ok = false;


            Pix pix = PixConverter.ToPix(bmp);

            var page = engine.Process(pix);

            string text = page.GetText();
            string[] lines = text.Split(
                    new[] { '\n' },
                    StringSplitOptions.None
                );
            // Proceso las líneas
            try
            {
                string[] final = new string[3];
                int j = 2;
                for (int i = lines.Length - 1; i >= 0; i--)
                {
                    if (j < 0)
                        break;
                    if (lines[i].Length == 0)
                        continue;
                    final[j] = lines[i];
                    if (final[j].Length > 30)
                        final[j] = final[j].Substring(final[j].Length - 30);
                    j--;
                }
                Dispatcher.Invoke((Action)(() =>
                {

                    txtOCR3.Text = final[2];
                    txtOCR2.Text = final[1];
                    txtOCR1.Text = final[0];

                    ok = DNI.DNI.validOCR(final);
                    if (ok)
                    {
                        txtOCR1.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255));
                        txtOCR1.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(200, 200, 200));
                        txtOCR2.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255));
                        txtOCR2.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(200, 200, 200));
                        txtOCR3.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255));
                        txtOCR3.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(200, 200, 200));
                    }
                    else
                    {
                        txtOCR1.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 41));
                        txtOCR1.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                        txtOCR2.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 41));
                        txtOCR2.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                        txtOCR3.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 41));
                        txtOCR3.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                    }
                }));
            }
            catch (Exception ex)
            {
                txtOCR3.Text = "";
                txtOCR2.Text = "";
                txtOCR1.Text = "";

            }
            MessageBoxResult r;
            if (ok)
            {
                r = WpfMessageBox.Show("Verificación de OCR", "OCR ok!", MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Information);
            }
            else
            {
                string strMsg = "Fallo en la verificación del OCR.";
                strMsg += "\nDesea aceptarla, finalizar o reintentar?";
                r = WpfMessageBox.Show("Verificación de OCR", strMsg, MessageBoxButton.YesNoCancel, WpfMessageBox.MessageBoxImage.Error);
            }

            if (r == MessageBoxResult.Cancel)
                getImgOtro();
            else
            {
                try
                {
                    currDNI.saveToDB();
                }
                catch (Exception ex)
                {
                    WpfMessageBox.Show("Error en la base de datos", "Error al grabar los datos", MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);
                }
                taskArea.TaskText = "Proceso finalizado";
                taskArea.GoBtn.Visibility = Visibility.Visible;
                taskArea.BackBtn.Visibility = Visibility.Collapsed;
                taskArea.BusyInd.Visibility = Visibility.Collapsed;
                taskArea.GoFunc = endWork;
            }

        }

  


        #region FP
        private void getFP()
        {
            taskArea.CurrTaskID = "fingerCapture";
            taskArea.TaskText = "Ponga su dígito pulgar derecho en el lector de huella";
            taskArea.GoBtn.Content = "OK";
            taskArea.GoBtn.Visibility = Visibility.Visible;
            taskArea.GoBtn.Focus();
            taskArea.BackBtn.Visibility = Visibility.Visible;
            taskArea.BackFunc = getImgOtro;
            taskArea.BusyInd.Visibility = Visibility.Hidden;
            taskArea.GoFunc = loadFP;
        }

        private void loadFP()
        {
            taskArea.TaskText = "Leyendo huella ...";
            taskArea.GoBtn.Visibility = Visibility.Collapsed;
            taskArea.BackFunc = cancelFP;
            taskArea.BackBtn.Content = "Cancelar";

            try
            {
                if (BioEngineFactory.getBio().OnCaptureOk == null)
                    BioEngineFactory.getBio().OnCaptureOk += onFPCapture;
                BioEngineFactory.getBio().startReading();
            }
            catch (Exception ex)
            {
                WpfMessageBox.Show(ex.GetType().Name, ex.Message, MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);
                getImgOtro();
                return;
            }
        }

        public void cancelFP()
        {
            BioEngineFactory.getBio().resetReaders();
            getFP();
        }

        private void onFPCapture(Fmd fmd, Bitmap bmp)
        {
            BioEngineFactory.getBio().resetReaders();
            currDNI.loadFromSensor(fmd, bmp);
            Dispatcher.Invoke((Action)(() =>
            {
                fpFromRdr.setHuella(currDNI.WpfImgHuellaSensor, currDNI.QualityFromSensor);
                currDNI.OK = currDNI.Matches();
                DNI.DNI prevDNI = null;

                try
                {
                    prevDNI = DNI.DNI.loadByDNI(currDNI.Numero);
                }
                catch (Exception ex)
                {

                }

                string strMsg;
                MessageBoxButton bt;
                WpfMessageBox.MessageBoxImage im;

                if (currDNI.OK)
                {
                    strMsg = "Las huellas coinciden.";
                    bt = MessageBoxButton.OK;
                    im = WpfMessageBox.MessageBoxImage.Information;

                    if (prevDNI != null)
                    {
                        strMsg += "\nAdemás, hay un intento previo del día " + prevDNI.Ts.ToString();
                        strMsg += "\ncon resultado " + (prevDNI.OK ? "positivo" : "negativo");
                        if (prevDNI.Matches(currDNI.LastFP, prevDNI.LastFP))
                        {
                            strMsg += "\ny esta huella coincide con la tomada en ese momento";
                        }
                        else
                        {
                            strMsg += "\ny esta huella no coincide con la tomada en ese momento";
                            im = WpfMessageBox.MessageBoxImage.Warning;
                        }
                    }
                }
                else
                {
                    strMsg = "Las huellas no coinciden.";
                    bt = MessageBoxButton.YesNoCancel;
                    im = WpfMessageBox.MessageBoxImage.Error;
                    if (prevDNI != null)
                    {
                        strMsg += "\nAdemás, hay un intento previo del día " + prevDNI.Ts.ToString();
                        strMsg += "\ncon resultado " + (prevDNI.OK ? "positivo" : "negativo");
                        if (prevDNI.Matches(currDNI.LastFP, prevDNI.LastFP))
                        {
                            strMsg += "\ny esta huella coincide con la tomada en ese momento";
                        }
                        else
                        {
                            strMsg += "\ny esta huella no coincide con la tomada en ese momento";
                        }
                    }
                    strMsg += "\nDesea aceptarla, finalizar o reintentar?";
                }

                MessageBoxResult r = WpfMessageBox.Show("Comparación de huellas", strMsg, bt, im);

                if (r == MessageBoxResult.Cancel)
                    getFP();
                else
                {
                    try
                    {
                        currDNI.saveToDB();
                    }
                    catch (Exception ex)
                    {
                        WpfMessageBox.Show("Error en la base de datos", "Error al grabar los datos", MessageBoxButton.OK, WpfMessageBox.MessageBoxImage.Error);
                    }
                    taskArea.TaskText = "Proceso finalizado";
                    taskArea.GoBtn.Visibility = Visibility.Visible;
                    taskArea.BackBtn.Visibility = Visibility.Collapsed;
                    taskArea.BusyInd.Visibility = Visibility.Collapsed;
                    taskArea.GoFunc = endWork;
                }
            }));

        }

        public void endWork()
        {
            Visibility = Visibility.Hidden;

            Application.Current.Shutdown();
        }


        #endregion
    }
}
