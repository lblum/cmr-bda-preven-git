﻿using NTwain;
using NTwain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Drawing;

namespace BDAPreven
{
    public class Scanner
    {
        public DataSource Device { get; protected set; } = null;
        public bool DevicePresent { get; protected set; } = false;
        public string LastException { get; private set; }
        public System.Drawing.Image LastImage { get; private set; }

        public delegate void EvtHandler();
        public EvtHandler OkHandler { get; set; } = null;
        public EvtHandler ErrHandler { get; set; } = null;

        public Scanner()
        {
            var appId = TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly());

            TwainSession session = new TwainSession(appId);
            session.TransferReady += trReady;
            session.DataTransferred += dataTtr;
            session.TransferError += errDel;
            
            session.Open();

            string devStr = Properties.Settings.Default.devStr;
            foreach (DataSource ds in session)
            {
                if (ds.Name.Contains(devStr))
                {
                    Device = ds;
                    Device.Open();
                    break;
                }
            }

            DevicePresent = Device != null && Device.IsOpen;

            if (DevicePresent)
            {
                try
                {
                    Device.Capabilities.ICapPixelType.SetValue(PixelType.RGB);
                    Device.Capabilities.ICapAutomaticRotate.SetValue(Properties.Settings.Default.AutomaticRotate ? BoolType.True : BoolType.False);
                    AutoSize doAutoSize;
                    if (Properties.Settings.Default.AutomaticBorderDetection)
                        doAutoSize = AutoSize.Auto;
                    else
                        doAutoSize = AutoSize.None;
                    Device.Capabilities.ICapAutoSize.SetValue(doAutoSize);
                    Device.Capabilities.ICapXResolution.SetValue((float)Properties.Settings.Default.Dpi);
                    Device.Capabilities.ICapYResolution.SetValue((float)Properties.Settings.Default.Dpi);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void doErr(string err)
        {
            LastException = err;
            ErrHandler?.Invoke();
        }

        private void errDel(object sender, TransferErrorEventArgs e)
        {
            doErr(e.Exception.Message);
        }


        private void dataTtr(object sender, DataTransferredEventArgs e)
        {
            try
            {
                LastImage = null;
                if (e.NativeData != IntPtr.Zero)
                {
                    var stream = e.GetNativeImageStream();
                    if (stream != null)
                    {
                        LastImage = System.Drawing.Image.FromStream(stream);
                    }
                } else
                {
                    throw new Exception("Problemas en la captura de la imagen");
                }

                OkHandler?.Invoke();
            }
            catch(Exception ex)
            {
                doErr(ex.Message);
            }
        }

        private void trReady(object sender, TransferReadyEventArgs e)
        {
            if (e.PendingTransferCount >= 0)
            {
                e.CancelAll = true;
                doErr("Carga cancelada por el usuario");
            }
        }

        public void startScan()
        {
            LastException = "";
            SourceEnableMode sem;
            if (Properties.Settings.Default.ShowTwainUI)
                sem = SourceEnableMode.ShowUI;
            else
                sem = SourceEnableMode.NoUI;
            Device.Enable(sem, false, (IntPtr)0);
        }
    }
}
