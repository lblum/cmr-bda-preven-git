﻿using BDAPreven.DNI;
using BDAPreven.Singleton;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BDA_preven_PDF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GlobalConfig.dbDriver = Properties.Settings.Default.dbDriver;
            GlobalConfig.dbDSN = Properties.Settings.Default.dbDSN;
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            //
            SaveFileDialog fileSave = new SaveFileDialog();
            fileSave.Filter = "Archivos CSV (*.csv) |*.csv";
            fileSave.DefaultExt = "csv";
            string ls = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (fileSave.ShowDialog() == true)
            {
                try
                {
                    List<int> dniList = DNI.loadIdsByDate(txtDNI.Text, fechaDesde.Value, fechaHasta.Value);
                    if (dniList.Count == 0)
                        throw new Exception("No hay datos con esas características");
                    StreamWriter fileWr = new StreamWriter(fileSave.FileName);
                    fileWr.Write("\"Fecha y hora\""); fileWr.Write(ls);
                    fileWr.Write("\"Operador\""); fileWr.Write(ls);
                    fileWr.Write("\"Nro. DNI\""); fileWr.Write(ls);
                    fileWr.Write("\"Apellidos\""); fileWr.Write(ls);
                    fileWr.Write("\"Nombres\""); fileWr.Write(ls);
                    fileWr.Write("\"Sexo\""); fileWr.Write(ls);
                    fileWr.Write("\"Nacionalidad\""); fileWr.Write(ls);
                    fileWr.Write("\"Fecha de nacimiento\""); fileWr.Write(ls);
                    fileWr.Write("\"Huella OK\""); fileWr.Write(ls);
                    fileWr.WriteLine();

                    foreach (int id in dniList)
                    {
                        DNI d = DNI.loadByID(id);
                        fileWr.Write("\"" + d.Ts.ToString("dd/MM/yyyy HH:mm:ss") + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.IdUser + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.Numero + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.Apellidos + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.Nombres + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.Sexo + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.Nacionalidad + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + d.FechaNacimiento + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + (d.OK ? "OK" : "Mala") + "\""); fileWr.Write(ls);
                        fileWr.WriteLine();
                    }
                    fileWr.Close();
                    MessageBox.Show("Exportación finalizada", "Info", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void btnPdf_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileSave = new SaveFileDialog();
            fileSave.Filter = "Archivos PDF (*.pdf) |*.pdf";
            fileSave.DefaultExt = "pdf";
            if (fileSave.ShowDialog() == true)
            {
                try
                {
                    List<int> dniList = DNI.loadIdsByDate(txtDNI.Text, fechaDesde.Value, fechaHasta.Value);
                    if (dniList.Count == 0)
                        throw new Exception("No hay datos con esas características");
                    PDFFile pdfFile = new PDFFile(fileSave.FileName, dniList);
                    MessageBox.Show("Exportación finalizada", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void txtDNI_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[0-9]+");
            e.Handled = !regex.IsMatch(e.Text);

        }
    }
}
