﻿
using BDAPreven.DNI;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.IO;
using System;
using BDAPreven;

namespace BDA_preven_PDF
{
    public class PDFFile
    {
        private Document pdfDoc = new Document(PageSize.A4);

        private Font _standardFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);

        public PDFFile(string fileName, List<int> dniList)
        {
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(fileName, FileMode.Create));
            pdfDoc.Open();

            PdfPTable tblResumen = new PdfPTable(5);
            tblResumen.WidthPercentage = 100;

            float[] colWidths = new float[] { 200, 200 , 200 , 600, 100 };
            tblResumen.SetWidths(colWidths);

            PdfPCell clUser = new PdfPCell(new Phrase("Operador", _standardFont));
            clUser.BorderWidth = 0;
            clUser.BorderWidthBottom = 0.75f;

            PdfPCell clDNI = new PdfPCell(new Phrase("DNI nro", _standardFont));
            clDNI.BorderWidth = 0;
            clDNI.BorderWidthBottom = 0.75f;

            PdfPCell clNombre = new PdfPCell(new Phrase("Apellido(s) y nombre(s)", _standardFont));
            clNombre.BorderWidth = 0;
            clNombre.BorderWidthBottom = 0.75f;

            PdfPCell clEjemplar = new PdfPCell(new Phrase("Ejemplar", _standardFont));
            clEjemplar.BorderWidth = 0;
            clEjemplar.BorderWidthBottom = 0.75f;

            PdfPCell clOk = new PdfPCell(new Phrase("OK?", _standardFont));
            clOk.BorderWidth = 0;
            clOk.BorderWidthBottom = 0.75f;

            tblResumen.AddCell(clUser);
            tblResumen.AddCell(clDNI);
            tblResumen.AddCell(clEjemplar);
            tblResumen.AddCell(clNombre);
            tblResumen.AddCell(clOk);

            int nPages = 0;
            foreach (int id in dniList)
            {
                DNI dni = DNI.loadByID(id);
                nPages++;
                clUser = new PdfPCell(new Phrase(dni.IdUser , _standardFont));
                clUser.BorderWidth = 0;
                clDNI = new PdfPCell(new Phrase(dni.Numero, _standardFont));
                clDNI.BorderWidth = 0;
                clEjemplar = new PdfPCell(new Phrase(dni.Ejemplar, _standardFont));
                clEjemplar.BorderWidth = 0;
                clNombre = new PdfPCell(new Phrase(dni.Apellidos + "," + dni.Nombres, _standardFont));
                clNombre.BorderWidth = 0;
                clOk = new PdfPCell(new Phrase(dni.OK ? "SI" : "NO", _standardFont));
                clOk.BorderWidth = 0;

                tblResumen.AddCell(clUser);
                tblResumen.AddCell(clDNI);
                tblResumen.AddCell(clEjemplar);
                tblResumen.AddCell(clNombre);
                tblResumen.AddCell(clOk);

                PdfPTable tblDetalle = new PdfPTable(1);
                addData(dni, tblDetalle);
                addImgData(dni, tblDetalle);
                pdfDoc.Add(tblDetalle);
                pdfDoc.NewPage();

            }
            pdfDoc.Add(tblResumen);

            pdfDoc.Close();
            writer.Close();

        }


        private void addData(DNI dni, PdfPTable tblDetalle)
        {
            PdfPTable data = new PdfPTable(2);
            addDataLine(data, "Operador", dni.IdUser);
            string dtStr = dni.Ts.ToString();
            DateTime dt = DateTime.Parse(dtStr);
            addDataLine(data, "Generado el ", dt.ToString("dd/MM/yyyy HH:mm:ss"));
            addDataLine(data, "DNI nro.", dni.Numero);
            addDataLine(data, "Ejemplar", dni.Ejemplar);
            addDataLine(data, "Apellido(s)", dni.Apellidos);
            addDataLine(data, "Nombre(s)", dni.Nombres);
            addDataLine(data, "Sexo", dni.Sexo);
            addDataLine(data, "Nacionalidad", dni.Nacionalidad);
            try
            {
                dtStr = dni.FechaNacimiento.ToString();
                dt = DateTime.Parse(dtStr);
                addDataLine(data, "Fecha de nacimiento", dt.ToString("dd/MM/yyyy"));
            }
            catch (Exception ex)
            {
                addDataLine(data, "Fecha de nacimiento", "");
            }
            addDataLine(data, "Ok?", dni.OK ? "SI" : "NO");
            tblDetalle.AddCell(new PdfPCell(data));

        }

        protected void addDataLine(PdfPTable tbl, string label, string texto)
        {
            tbl.AddCell(
                new PdfPCell
                {
                    Phrase = new Phrase(label, _standardFont),
                    HorizontalAlignment = Element.ALIGN_RIGHT
                }
                );
            tbl.AddCell(
                new PdfPCell
                {
                    Phrase = new Phrase(texto, _standardFont),
                    HorizontalAlignment = Element.ALIGN_LEFT
                }
                );

        }

        private void addImgData(DNI dni, PdfPTable tblDetalle)
        {
            PdfPTable data = new PdfPTable(2);
            data.DefaultCell.Border = Rectangle.NO_BORDER;

            Image img;


            PdfPCell clFirma = new PdfPCell(new Phrase("Firma", _standardFont));
            clFirma.BorderWidth = 0;
            clFirma.BorderWidthBottom = 0.75f;
            clFirma.HorizontalAlignment = Element.ALIGN_RIGHT;
            data.AddCell(clFirma);


            img = Image.GetInstance(dni.Firma, System.Drawing.Imaging.ImageFormat.Bmp);
            img.ScaleToFit(200, 200);
            data.AddCell(new PdfPCell(img));

            img = Image.GetInstance(dni.Anverso, System.Drawing.Imaging.ImageFormat.Png);
            img.ScaleToFit(200, 200);
            data.AddCell(new PdfPCell(img));

            img = Image.GetInstance(dni.Reverso, System.Drawing.Imaging.ImageFormat.Bmp);
            img.ScaleToFit(200, 200);
            data.AddCell(new PdfPCell(img));

            PdfPTable tblHuellas = new PdfPTable(4);
            tblHuellas.DefaultCell.Border = Rectangle.NO_BORDER;

            foreach (Huella h in dni.HuellasSensor)
            {
                img = Image.GetInstance(h.Img.Bitmap, System.Drawing.Imaging.ImageFormat.Bmp);
                img.ScaleToFit(100F, 100F);
                PdfPCell cell = new PdfPCell(img);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.Border = Rectangle.NO_BORDER;
                cell.BorderWidth = 2f;
                cell.BorderColor = h.OK ? BaseColor.GREEN : BaseColor.RED;
                tblHuellas.AddCell(cell);
            }
            tblHuellas.CompleteRow();
            tblDetalle.AddCell(data);
            tblDetalle.AddCell(tblHuellas);
        }


    }
}
