﻿using BDAPreven;
using System.Windows;

namespace BDA_preven_PDF
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Splash splashWindow = new Splash();
            splashWindow.mainWindow = new MainWindow();
            Application.Current.MainWindow = splashWindow;
            Application.Current.MainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }
    }
}
