﻿using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace BDAPreven.WPFControls
{
    /// <summary>
    /// Lógica de interacción para ImgHuella.xaml
    /// </summary>
    public partial class ImgHuella : UserControl
    {
        public ImgHuella()
        {
            InitializeComponent();
        }

        public void setHuella(BitmapSource image,int q)
        {
            if (image != null)
            {
                img.Source = image;
                qlt.Value = q;
            }
        }
        
    }
}
