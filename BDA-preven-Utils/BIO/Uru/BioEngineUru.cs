﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDAPreven.BIO.Uru

{
    class BioEngineUru
    {
        /*
                private ReaderCollection _readers = null;

                private static BioEngine Fbio = null;
                private static object syncRoot = new Object();


                private BioEngine()
                {

                }

                public static BioEngine getBio()
                {
                    if (Fbio == null)
                    {
                        lock (syncRoot)
                        {
                            if (Fbio == null)
                            {
                                Fbio = new BioEngine();
                            }
                        }
                    }
                    return Fbio;
                }

                public void startReading()
                {
                    _readers = ReaderCollection.GetReaders();

                    if (_readers.Count == 0)
                        throw new Exception("No hay ningún lector instalado");

                    foreach (Reader rdr in _readers)
                    {

                        Constants.ResultCode result = Constants.ResultCode.DP_DEVICE_FAILURE;

                        result = rdr.Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                        if (result != Constants.ResultCode.DP_SUCCESS)
                        {
                            rdr.Dispose();
                            throw new Exception("Problemas al abrir el lector: " + result.ToString());
                        }

                        rdr.On_Captured += new Reader.CaptureCallback(OnCaptured);
                        try
                        {
                            Constants.ResultCode captureResult = rdr.CaptureAsync(Constants.Formats.Fid.ANSI, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, 500);
                            if (captureResult != Constants.ResultCode.DP_SUCCESS)
                                throw new Exception("Error en la captura:" + captureResult);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error en la captura:" + ex.Message);
                        }

                    }

                }

                public delegate void OnCaptureOKCallback(Fid fid);

                public OnCaptureOKCallback OnCaptureOk = null;

                private void OnCaptured(CaptureResult captureResult)
                {
                    if (!checkCaptureResult(captureResult))
                        return;

                    if (OnCaptureOk != null)
                    {
                        if (OnCaptureOk.Target is System.Windows.Controls.Control)
                            (OnCaptureOk.Target as System.Windows.Controls.Control).Dispatcher.Invoke(OnCaptureOk, captureResult.Data);
                    }

                }

                protected bool checkCaptureResult(CaptureResult captureResult)
                {
                    if (captureResult.Data == null || captureResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                    {
                        if (captureResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                            throw new Exception(captureResult.ResultCode.ToString());

                        if ((captureResult.Quality != Constants.CaptureQuality.DP_QUALITY_CANCELED))
                            throw new Exception("Calidad baja en la captura (" + captureResult.Quality + ")");
                        return false;
                    }
                    return true;
                }

                public void resetReaders()
                {
                    foreach (Reader rdr in _readers)
                    {
                        rdr.Reset();
                    }

                }
            }
        */
    }
}
