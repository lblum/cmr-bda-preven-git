﻿using System;

namespace BDAPreven.BIO.Eikon
{
    public class EikonRdrErrors
    {
        // Success return status
        public const int PT_STATUS_OK = 0;
        // General or unknown error status. It is also possible that the function
        //only partially succeeded, and that the device is in an inconsistent state.
        public const int PT_STATUS_GENERAL_ERROR = -1001;

        // PerfectTrust API wasn't initialized
        public const int PT_STATUS_API_NOT_INIT = -1002;

        // PerfectTrust API has been already initialized
        public const int PT_STATUS_API_ALREADY_INITIALIZED = -1003;

        // Invalid parameter error
        public const int PT_STATUS_INVALID_PARAMETER = -1004;

        // Invalid handle error
        public const int PT_STATUS_INVALID_HANDLE = -1005;

        // Not enough memory to process given operation
        public const int PT_STATUS_NOT_ENOUGH_MEMORY = -1006;

        // Failure of extern memory allocation function
        public const int PT_STATUS_MALLOC_FAILED = -1007;

        // Passed data are too large
        public const int PT_STATUS_DATA_TOO_LARGE = -1008;

        // Not enough permanent memory to store data
        public const int PT_STATUS_NOT_ENOUGH_PERMANENT_MEMORY = -1009;

        // There is more data to return than the supplied buffer can contain
        public const int PT_STATUS_MORE_DATA = -1010;

        // Function failed
        public const int PT_STATUS_FUNCTION_FAILED = -1033;

        // Invalid form of PT_INPUT_BIR structure
        public const int PT_STATUS_INVALID_INPUT_BIR_FORM = -1036;

        // TFM has returned wrong or unexpected response
        public const int PT_STATUS_WRONG_RESPONSE = -1037;

        // Not enough memory on TFM to process given operation
        public const int PT_STATUS_NOT_ENOUGH_TFM_MEMORY = -1038;

        // Connection is already opened
        public const int PT_STATUS_ALREADY_OPENED = -1039;

        // Cannot connect to TFM
        public const int PT_STATUS_CANNOT_CONNECT = -1040;

        // Timeout elapsed
        public const int PT_STATUS_TIMEOUT = -1041;

        // Bad biometric template
        public const int PT_STATUS_BAD_BIO_TEMPLATE = -1042;

        // Requested slot was not found
        public const int PT_STATUS_SLOT_NOT_FOUND = -1043;

        // Attempt to export antispoofing info from TFM
        public const int PT_STATUS_ANTISPOOFING_EXPORT = -1044;

        // Attempt to import antispoofing info to TFM
        public const int PT_STATUS_ANTISPOOFING_IMPORT = -1045;

        // Access to operation is denied
        public const int PT_STATUS_ACCESS_DENIED = -1046;

        // No template was captured in current session
        public const int PT_STATUS_NO_TEMPLATE = -1049;

        // Timeout for biometric operation has expired
        public const int PT_STATUS_BIOMETRIC_TIMEOUT = -1050;

        // Failure of template consolidation
        public const int PT_STATUS_CONSOLIDATION_FAILED = -1051;

        // Biometric operation canceled
        public const int PT_STATUS_BIO_OPERATION_CANCELED = -1052;

        // Authentification failed
        public const int PT_STATUS_AUTHENTIFICATION_FAILED = -1053;

        // Unknown command
        public const int PT_STATUS_UNKNOWN_COMMAND = -1054;

        // Power off attempt failed
        public const int PT_STATUS_GOING_TO_SLEEP = -1055;

        // Function or service is not implemented
        public const int PT_STATUS_NOT_IMPLEMENTED = -1056;

        // General communication error
        public const int PT_STATUS_COMM_ERROR = -1057;

        // Session was terminated
        public const int PT_STATUS_SESSION_TERMINATED = -1058;

        // Touch chip error occured
        public const int PT_STATUS_TOUCH_CHIP_ERROR = -1059;

        // I2C EEPROM error occured
        public const int PT_STATUS_I2C_EEPROM_ERROR = -1060;

        // Purpose parameter (or BIR's purpose) is invalid for given
        public const int PT_STATUS_INVALID_PURPOSE = -1061;

        // Finger swipe is too bad for image reconstruction
        public const int PT_STATUS_SWIPE_TOO_BAD = -1062;

        // Value of parameter is not supported
        public const int PT_STATUS_NOT_SUPPORTED = -1063;

        // Calibration failed
        public const int PT_STATUS_CALIBRATION_FAILED = -1064;

        // Antispoofing data were not captured
        public const int PT_STATUS_ANTISPOOFING_NOT_CAPTURED = -1065;

        // Sensor latch-up event detected
        public const int PT_STATUS_LATCHUP_DETECTED = -1066;

        // Diagnostics failed
        public const int PT_STATUS_DIAGNOSTICS_FAILED = -1067;

        // Attempt to upgrade to same firmware version
        public const int PT_STATUS_SAME_VERSION = -1068;

        // No sensor
        public const int PT_STATUS_NO_SENSOR = -1069;

        // The measured values are out of allowable limits
        public const int PT_STATUS_SENSOR_OUT_OF_LIMITS = -1070;

        // Too many bad lines
        public const int PT_STATUS_TOO_MANY_BAD_LINES = -1071;

        // Sensor is not repairable
        public const int PT_STATUS_SENSOR_NOT_REPAIRABLE = -1072;

        // Gain offset calibration error
        public const int PT_STATUS_GAIN_OFFSET = -1073;

        // Asynchronous power shut down
        public const int PT_STATUS_POWER_SHUTOFF = -1074;

        // Attempt to upgrade to older firmware version
        public const int PT_STATUS_OLD_VERSION = -1075;

        // Connection interrupted because of suspend request
        public const int PT_STATUS_SUSPEND = -1076;

        // Device not found
        public const int PT_STATUS_DEVICE_NOT_FOUND = -1077;

        // Device doesn't work as expected
        public const int PT_STATUS_DEVICE_SICK = -1078;

        // Host hardware doesn't support requested communication speed
        public const int PT_STATUS_UNSUPPORTED_SPEED = -1079;

        // Sensor is not calibrated
        public const int PT_STATUS_SENSOR_NOT_CALIBRATED = -1080;

        // Firmware is missing or corrupted, device is running in safe mode
        public const int PT_STATUS_SAFE_MODE = -1081;

        // Sensor hardware error occured
        public const int PT_STATUS_SENSOR_HW_ERROR = -1082;

        // Session was not authenticated yet
        public const int PT_STATUS_SESSION_NOT_AUTHENTICATED = -1083;

        // Secure channel has been already established
        public const int PT_STATUS_SECURE_CHANNEL_ALREADY_ESTABLISHED = -1084;

        // Overflow of One Time Password sequence number
        public const int PT_STATUS_OTP_SEQUENCE_NUMBER_OVERFLOW = -1085;

        // General NVM error
        public const int PT_STATUS_NVM_ERROR = -1086;

        // NVM write operation failed
        public const int PT_STATUS_NVM_CANNOT_WRITE = -1087;

        // NVM read operation failed
        public const int PT_STATUS_NVM_CANNOT_READ = -1088;

        // Attempt to access non-existing internal NVM file
        public const int PT_STATUS_NVM_INVALID_FILE_ID = -1089;

        // General crypto error
        public const int PT_STATUS_CRYPTO_ERROR = -1090;

        // Requested cryptographic mechanism is not supported
        public const int PT_STATUS_CRYPTO_MECHANISM_NOT_SUPPORTED = -1091;

        // Padding error detected during crypto operation
        public const int PT_STATUS_CRYPTO_PADDING_ERROR = -1092;

        // Key too long (probably due to the export regulations)
        public const int PT_STATUS_CRYPTO_KEY_TOO_LONG = -1093;

        // Bad symmetric key used
        public const int PT_STATUS_CRYPTO_SYM_BAD_KEY = -1094;

        // HW random number generator initialization failed
        public const int PT_STATUS_HW_RNG_INIT_ERROR = -1095;

        // Device is already opened for exclusive access by somebody else
        public const int PT_STATUS_EX_ACCESS_VIOLATION = -1096;

        // Used wrong finger data access rights
        public const int PT_STATUS_WRONG_FINGER_DATA_ACCESS_RIGHTS = -1097;

        // Last match data don't exist
        public const int PT_STATUS_NO_LAST_MATCH_DATA = -1098;

        // No data found
        public const int PT_STATUS_NO_DATA = -1099;

        // General smart-card error
        public const int PT_STATUS_SC_ERROR = -3100;

        // Communication with this card is not supported
        public const int PT_STATUS_SC_NOT_SUPPORTED = -3101;

        // Failure during communication with the card
        public const int PT_STATUS_SC_COMM_FAIL = -3102;

        // Incorrect parameter detected
        public const int PT_STATUS_SC_BAD_PARAM = -3103;

        // the card is not present in the reader
        public const int PT_STATUS_SC_NO_CARD = -3104;

        public static string getErrMsg(int errNo)
        {
            string errStr = "";
            switch (errNo)
            {
                case PT_STATUS_OK:
                    errStr = "Function successful";
                    break;

                case PT_STATUS_GENERAL_ERROR:
                    errStr = "General Error";
                    break;
                case PT_STATUS_API_NOT_INIT:
                    errStr = "PerfectTrust API wasn't initialized";
                    break;
                case PT_STATUS_API_ALREADY_INITIALIZED:
                    errStr = "PerfectTrust API has been already initialized";
                    break;
                case PT_STATUS_INVALID_PARAMETER:
                    errStr = "Invalid parameter error";
                    break;
                case PT_STATUS_INVALID_HANDLE:
                    errStr = "Invalid handle error";
                    break;
                case PT_STATUS_NOT_ENOUGH_MEMORY:
                    errStr = "Not enough memory to process given operation";
                    break;
                case PT_STATUS_MALLOC_FAILED:
                    errStr = "Failure of extern memory allocation function";
                    break;
                case PT_STATUS_DATA_TOO_LARGE:
                    errStr = "Passed data are too large";
                    break;
                case PT_STATUS_NOT_ENOUGH_PERMANENT_MEMORY:
                    errStr = "Not enough permanent memory to store data";
                    break;
                case PT_STATUS_MORE_DATA:
                    errStr = "There is more data to return than the supplied buffer can contain";
                    break;
                case PT_STATUS_FUNCTION_FAILED:
                    errStr = "Function failed";
                    break;
                case PT_STATUS_INVALID_INPUT_BIR_FORM:
                    errStr = "Invalid form of PT_INPUT_BIR structure";
                    break;
                case PT_STATUS_WRONG_RESPONSE:
                    errStr = "TFM has returned wrong or unexpected response";
                    break;
                case PT_STATUS_NOT_ENOUGH_TFM_MEMORY:
                    errStr = "Not enough memory on TFM to process given operation";
                    break;
                case PT_STATUS_ALREADY_OPENED:
                    errStr = "Connection is already opened";
                    break;
                case PT_STATUS_CANNOT_CONNECT:
                    errStr = "Cannot connect to TFM";
                    break;
                case PT_STATUS_TIMEOUT:
                    errStr = "Timeout elapsed";
                    break;
                case PT_STATUS_BAD_BIO_TEMPLATE:
                    errStr = "Bad biometric template";
                    break;
                case PT_STATUS_SLOT_NOT_FOUND:
                    errStr = "Requested slot was not found";
                    break;
                case PT_STATUS_ANTISPOOFING_EXPORT:
                    errStr = "Attempt to export antispoofing info from TFM";
                    break;
                case PT_STATUS_ANTISPOOFING_IMPORT:
                    errStr = "Attempt to import antispoofing info to TFM";
                    break;
                case PT_STATUS_ACCESS_DENIED:
                    errStr = "Access to operation is denied";
                    break;
                case PT_STATUS_NO_TEMPLATE:
                    errStr = "No template was captured in current session";
                    break;
                case PT_STATUS_BIOMETRIC_TIMEOUT:
                    errStr = "Timeout for biometric operation has expired";
                    break;
                case PT_STATUS_CONSOLIDATION_FAILED:
                    errStr = "Failure of template consolidation";
                    break;
                case PT_STATUS_BIO_OPERATION_CANCELED:
                    errStr = "Biometric operation canceled";
                    break;
                case PT_STATUS_AUTHENTIFICATION_FAILED:
                    errStr = "Authentification failed";
                    break;
                case PT_STATUS_UNKNOWN_COMMAND:
                    errStr = "Unknown command";
                    break;
                case PT_STATUS_GOING_TO_SLEEP:
                    errStr = "Power off attempt failed";
                    break;
                case PT_STATUS_NOT_IMPLEMENTED:
                    errStr = "Function or service is not implemented";
                    break;
                case PT_STATUS_COMM_ERROR:
                    errStr = "General communication error";
                    break;
                case PT_STATUS_SESSION_TERMINATED:
                    errStr = "Session was terminated";
                    break;
                case PT_STATUS_TOUCH_CHIP_ERROR:
                    errStr = "Touch chip error occured";
                    break;
                case PT_STATUS_I2C_EEPROM_ERROR:
                    errStr = "I2C EEPROM error occured";
                    break;
                case PT_STATUS_INVALID_PURPOSE:
                    errStr = " Purpose parameter (or BIR's purpose) is invalid for given ";
                    break;
                case PT_STATUS_SWIPE_TOO_BAD:
                    errStr = " Finger swipe is too bad for image reconstruction";
                    break;
                case PT_STATUS_NOT_SUPPORTED:
                    errStr = " Value of parameter is not supported";
                    break;
                case PT_STATUS_CALIBRATION_FAILED:
                    errStr = " Calibration failed";
                    break;
                case PT_STATUS_ANTISPOOFING_NOT_CAPTURED:
                    errStr = " Antispoofing data were not captured";
                    break;
                case PT_STATUS_LATCHUP_DETECTED:
                    errStr = " Sensor latch-up event detected";
                    break;
                case PT_STATUS_DIAGNOSTICS_FAILED:
                    errStr = " Diagnostics failed";
                    break;
                case PT_STATUS_SAME_VERSION:
                    errStr = " Attempt to upgrade to same firmware version";
                    break;
                case PT_STATUS_NO_SENSOR:
                    errStr = " No sensor";
                    break;
                case PT_STATUS_SENSOR_OUT_OF_LIMITS:
                    errStr = " The measured values are out of allowable limits";
                    break;
                case PT_STATUS_TOO_MANY_BAD_LINES:
                    errStr = " Too many bad lines";
                    break;
                case PT_STATUS_SENSOR_NOT_REPAIRABLE:
                    errStr = " Sensor is not repairable";
                    break;
                case PT_STATUS_GAIN_OFFSET:
                    errStr = " Gain offset calibration error";
                    break;
                case PT_STATUS_POWER_SHUTOFF:
                    errStr = " Asynchronous power shut down";
                    break;
                case PT_STATUS_OLD_VERSION:
                    errStr = " Attempt to upgrade to older firmware version";
                    break;
                case PT_STATUS_SUSPEND:
                    errStr = " Connection interrupted because of suspend request";
                    break;
                case PT_STATUS_DEVICE_NOT_FOUND:
                    errStr = " Device not found";
                    break;
                case PT_STATUS_DEVICE_SICK:
                    errStr = " Device doesn't work as expected";
                    break;
                case PT_STATUS_UNSUPPORTED_SPEED:
                    errStr = " Host hardware doesn't support requested communication speed";
                    break;
                case PT_STATUS_SENSOR_NOT_CALIBRATED:
                    errStr = " Sensor is not calibrated";
                    break;
                case PT_STATUS_SAFE_MODE:
                    errStr = " Firmware is missing or corrupted, device is running in safe mode";
                    break;
                case PT_STATUS_SENSOR_HW_ERROR:
                    errStr = " Sensor hardware error occured";
                    break;
                case PT_STATUS_SESSION_NOT_AUTHENTICATED:
                    errStr = " Session was not authenticated yet";
                    break;
                case PT_STATUS_SECURE_CHANNEL_ALREADY_ESTABLISHED:
                    errStr = " Secure channel has been already established";
                    break;
                case PT_STATUS_OTP_SEQUENCE_NUMBER_OVERFLOW:
                    errStr = " Overflow of One Time Password sequence number";
                    break;
                case PT_STATUS_NVM_ERROR:
                    errStr = " General NVM error";
                    break;
                case PT_STATUS_NVM_CANNOT_WRITE:
                    errStr = " NVM write operation failed";
                    break;
                case PT_STATUS_NVM_CANNOT_READ:
                    errStr = " NVM read operation failed";
                    break;
                case PT_STATUS_NVM_INVALID_FILE_ID:
                    errStr = " Attempt to access non-existing internal NVM file";
                    break;
                case PT_STATUS_CRYPTO_ERROR:
                    errStr = " General crypto error";
                    break;
                case PT_STATUS_CRYPTO_MECHANISM_NOT_SUPPORTED:
                    errStr = " Requested cryptographic mechanism is not supported";
                    break;
                case PT_STATUS_CRYPTO_PADDING_ERROR:
                    errStr = " Padding error detected during crypto operation";
                    break;
                case PT_STATUS_CRYPTO_KEY_TOO_LONG:
                    errStr = " Key too long (probably due to the export regulations)";
                    break;
                case PT_STATUS_CRYPTO_SYM_BAD_KEY:
                    errStr = " Bad symmetric key used";
                    break;
                case PT_STATUS_HW_RNG_INIT_ERROR:
                    errStr = " HW random number generator initialization failed";
                    break;
                case PT_STATUS_EX_ACCESS_VIOLATION:
                    errStr = " Device is already opened for exclusive access by somebody else";
                    break;
                case PT_STATUS_WRONG_FINGER_DATA_ACCESS_RIGHTS:
                    errStr = " Used wrong finger data access rights";
                    break;
                case PT_STATUS_NO_LAST_MATCH_DATA:
                    errStr = " Last match data don't exist";
                    break;
                case PT_STATUS_NO_DATA:
                    errStr = " No data found";
                    break;
                case PT_STATUS_SC_ERROR:
                    errStr = " General smart-card error";
                    break;
                case PT_STATUS_SC_NOT_SUPPORTED:
                    errStr = " Communication with this card is not supported";
                    break;
                case PT_STATUS_SC_COMM_FAIL:
                    errStr = " Failure during communication with the card";
                    break;
                case PT_STATUS_SC_BAD_PARAM:
                    errStr = " Incorrect parameter detected";
                    break;
                case PT_STATUS_SC_NO_CARD:
                    errStr = " The card is not present in the reader";
                    break;
                default:
                    errStr = "Unrecognized Error";
                    break;
            }
            return errStr;
        }

        public static void check(int errNo)
        {
            if (errNo != PT_STATUS_OK)
            {
                throw new Exception(getErrMsg(errNo));
            }
        }
    }

}
