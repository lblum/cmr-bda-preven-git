﻿using DPUruNet;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BDAPreven.BIO.Eikon
{
    public class BioEngineEikon:BioEngine
    {
        EikonRdr rdr;
        public BioEngineEikon()
        {
            rdr = EikonRdr.getRdr();
        }

        public override void startReading()
        {
            rdr.OnCaptureOk = onCaptureOk;
            rdr.OnCaptureFail = onCaptureFail;
            rdr.startScan();
        }

        private void onCaptureFail(string errStr)
        {
            //throw new NotImplementedException();
        }

        private void onCaptureOk(Bitmap bmp)
        {
            Image<Gray, byte>  FimgFromSensor = new Image<Gray, byte>(bmp);
            DataResult<Fmd> rc = FeatureExtraction.CreateFmdFromRaw(getRawData(FimgFromSensor), 0, 0, FimgFromSensor.Width, FimgFromSensor.Height, 500, Constants.Formats.Fmd.ISO);

            if (rc.ResultCode == Constants.ResultCode.DP_SUCCESS && rc.Data != null)
            {
                OnCaptureOk?.Invoke(rc.Data,bmp);                
            }
            else
            {
                throw new Exception("Error en la binarización");
            }
        }

        public override void resetReaders()
        {
            EikonRdr.releaseRdr();
        }

        protected byte[] getRawData(Image<Gray, byte> img)
        {
            byte[] retVal = new byte[img.Width * img.Height];
            int i = 0;
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    retVal[i++] = img.Data[y, x, 0];
                }
            }
            return retVal;
        }
    }
}
