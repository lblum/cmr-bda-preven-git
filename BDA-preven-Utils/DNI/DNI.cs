﻿using BDA_preven.Singleton;
using BDAPreven.Singleton;
using DPUruNet;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using SourceAFIS.Templates;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace BDAPreven.DNI
{
    public class DNI
    {
        public long ID { get; protected set; } = -1;
        public DateTime Ts { get; protected set; }
        public string SerialData { get { return FSerialData; } }
        public string Numero { get { return getNumero(); } }
        public string Tipo { get { return getTipo(); } }
        public string Apellidos { get { return getApellidos(); } }
        public string Nombres { get { return getNombres(); } }
        public string Ejemplar { get { return getEjemplar(); } }
        public string Nacionalidad { get { return getNacionalidad(); } }
        public string FechaNacimiento { get { return getFechaNacimiento(); } }
        public string Sexo { get { return getSexoAsString(); } }
        public bool OK = true;
        public string ScData = "";
        public string IdUser = "";

        protected string FSerialData;

        private string getSexoAsString()
        {
            string s = this.getSexo();
            if (s.Equals("F"))
                return "Femenino";
            if (s.Equals("M"))
                return "Masculino";
            return "Otros";
        }

        protected virtual string getNumero() { return ""; }
        protected virtual string getTipo() { return ""; }
        protected virtual string getApellidos() { return ""; }
        protected virtual string getNombres() { return ""; }
        protected virtual string getNacionalidad() { return ""; }
        protected virtual string getEjemplar() { return getTipo(); }
        protected virtual string getFechaNacimiento() { return ""; }
        public virtual string getFechaNacimientoAnsi()
        {
            string retVal = FechaNacimiento;
            if (retVal.Length < 10)
                return "";
            string a = retVal.Substring(6);
            string m = retVal.Substring(3, 2);
            string d = retVal.Substring(0, 2);
            return a + m + d;
        }
        public virtual string getSexo() { return ""; }

        protected string getDataByPos(int pos)
        {
            if (pos < arrData.Length)
                return arrData[pos].Trim();
            else
                return "";
        }

        private string[] arrData;

        public DNI(string serialData)
        {
            this.FSerialData = serialData;

            arrData = serialData.Split('@');
        }

        /*
         * Los datos biométricos
         */

        // Medidas en milímetros
        protected const int dniW = 86;   // Ancho del DNI
        protected const int dniH = 54;   // Alto del DNI

        // Resolución default de los lectores
        protected const int DPIDef = 500;      // Puntos / pulgada
        protected const float ppmm = 25.4F;    // mm por pulgada

        // Los datos del sensor
        /* UrU 4500 
        public int sensorW = 350;
        public int sensorH = 380;
        */

        /* Eikon
        */
        protected const int sensorW = 256;
        protected const int sensorH = 360;

        // Los bitmaps originales a ser grabados en la base de datos
        protected Bitmap FAnverso = null;
        protected Bitmap FReverso = null;
        public Bitmap Anverso {
            get
            {
                // Reescalar, si corresponde
                return FAnverso;
            }
            set
            {
                FAnverso = value;
            }
        } // Acá esta el código de barras
        public Bitmap Reverso
        {
            get
            {
                // Reescalar, si corresponde
                return FReverso;
            }
            set
            {
                FReverso = value;
            }
        }  // Este es el otro lado

        // El rectángulo de la huella (en mm)
        protected Rectangle rectHuella = Rectangle.Empty;
        protected void setRectHuella(int t, int b, int l, int r)
        {
            rectHuella = new Rectangle(l, t, r - l, b - t);
        }
        // El rectángulo del OCR (en mm)
        protected Rectangle rectOCR = Rectangle.Empty;
        protected void setRectOCR(int t, int b, int l, int r)
        {
            rectOCR = new Rectangle(l, t, r - l, b - t);
        }

        // El rectángulo de la firma (en mm)
        protected Rectangle rectFirma = Rectangle.Empty;
        protected void setRectFirma(int t, int b, int l, int r)
        {
            rectFirma = new Rectangle(l, t, r - l, b - t);
        }

        // Indica donde se encuentra la huella
        public bool HuellaEnAnverso { get; protected set; } = true;

        // Indica donde se encuentra la firma
        public bool FirmaEnAnverso { get; protected set; } = true;

        public Bitmap Firma
        {
            get
            {
                // Ajusto el rectángulo por la resolución
                float rr = Resolucion / ppmm;
                Rectangle rect = new Rectangle();
                rect.X = (int)(rectFirma.X * rr);
                rect.Y = (int)(rectFirma.Y * rr);
                rect.Width = (int)(rectFirma.Width * rr);
                rect.Height = (int)(rectFirma.Height * rr);
                Image<Gray, byte> aux = null;
                aux = new Image<Gray, byte>(FirmaEnAnverso ? Anverso : Reverso);
                float scale = (float)DPIDef / (float)Resolucion;
                return aux.Copy(rect).Resize(scale, Inter.Linear).Bitmap;
            }
        }

        public Bitmap OCR
        {
            get
            {
                // Ajusto el rectángulo por la resolución
                float rr = Resolucion / ppmm;
                Rectangle rect = new Rectangle();
                rect.X = (int)(rectOCR.X * rr);
                rect.Y = (int)(rectOCR.Y * rr);
                rect.Width = (int)(rectOCR.Width * rr);
                rect.Height = (int)(rectOCR.Height * rr);
                Image<Gray, byte> aux = null;
                Image<Gray, byte> aux3 = null;
                float scale = (float)DPIDef / (float)Resolucion;
                aux = new Image<Gray, byte>(Reverso);
                aux3 = aux.Copy(rect).Resize(scale, Inter.Linear);
                //aux = new Image<Gray, byte>(aux3.Size);
                //CvInvoke.GaussianBlur(aux3, aux, new Size(13, 13), 2, 2);
                /*aux = new Image<Gray, byte>(aux3.Width, aux3.Height);
                CvInvoke.FastNlMeansDenoising(aux3, aux);*/
                return aux3.Dilate(1).Bitmap;
            }
        }

        // La resolución
        public int Resolucion { get; protected set; }

        // El template ppal
        public IsoFormat getMainTpl()
        {
            if (huellasScanner.Count <= 0)
            {
                int m = MaxDeltaZoom;
                MaxDeltaZoom = 1;
                setMinutiaeScanner();
                MaxDeltaZoom = m;
                if (huellasScanner.Count <= 0)
                    return null;
            }

            Huella h = huellasScanner[0];
            IsoFormat iso = new IsoFormat(h.Tpl.Bytes);
            return iso;
        }

        // Los templates escalados
        public List<IsoFormat> getScaledTpl() {
            return getScaledTpl(MaxDeltaZoom, 0.5F);
        }

        public List<IsoFormat> getScaledTpl(int maxZoom, float zoomStep)
        {
            List<IsoFormat> retVal = new List<IsoFormat>();
            int i, j;
            IsoFormat tpl = getMainTpl();

            for (i = 0; i < maxZoom; i++)
            {
                for (j = 0; j < maxZoom; j++)
                {
                    float scaleX = zoomStep * (float)i;
                    float scaleY = zoomStep * (float)j;
                    retVal.Add(tpl.zoom(scaleX, scaleY));
                }
            }
            return retVal;
        }


        public void loadBMPScanner(Bitmap bmp, int resolucion, bool esAnverso)
        {
            Resolucion = resolucion;
            if (esAnverso)
            {
                Anverso = bmp;
            }
            else
            {
                Reverso = bmp;
            }
        }

        protected Image<Gray, Byte> imgHuellaScanner = null;
        public Bitmap BmpHuellaScanner
        {
            get
            {
                if (imgHuellaScanner == null)
                    return null;
                return imgHuellaScanner.Bitmap;
            }
        }

        public BitmapSource WpfImgHuellaScanner
        {
            get
            {
                return ImgUtils.GetImageStream(imgHuellaScanner.Bitmap);
            }
        }

        // Las huellas reescaladas, movidas y deformadas
        List<Image<Gray, byte>> huellas = new List<Image<Gray, byte>>();
        public static int MaxDeltaZoom = 10;

        public void generateHuellas()
        {
            generateHuellas(false);
        }

        public void generateHuellas(bool soloUna)
        {

            // Ajusto el rectángulo por la resolución
            float rr = Resolucion / ppmm;
            Rectangle rect = new Rectangle();
            rect.X = (int)(rectHuella.X * rr);
            rect.Y = (int)(rectHuella.Y * rr);
            rect.Width = (int)(rectHuella.Width * rr);
            rect.Height = (int)(rectHuella.Height * rr);
            Image<Gray, byte> aux = null;
            aux = new Image<Gray, byte>(HuellaEnAnverso ? Anverso : Reverso);
            float scale = (float)DPIDef / (float)Resolucion;
            preprocesarHuella(aux.Copy(rect).Resize(scale, Inter.Linear));
            if (soloUna)
                return;

            Point cImg = ImgUtils.getCentroidFP(imgHuellaScanner);
            Point cTpl = ImgUtils.getCentroidTpl(imgHuellaScanner, Resolucion);


            generateHuellas(cImg.X, cImg.Y);
            if (!cTpl.Equals(cImg))
                generateHuellas(cTpl.X, cTpl.Y);

        }

        protected void preprocesarHuella(Image<Gray, byte> img)
        {

            imgHuellaScanner = img;//.ThresholdBinary(new Gray(128), new Gray(255));
            //imgHuellaScanner._EqualizeHist();
        }

        public static int MaxProfZoom = 2;

        public int QualityFromScanner { get; private set; } = 0;
        protected List<Huella> huellasScanner = new List<Huella>();
        public List<Huella> HuellasSensor { get; protected set; } = new List<Huella>();

        public bool RecortarHuella = true;

        private void generateHuellas(int c_x, int c_y)
        {

            int X = c_x;
            int Y = c_y;
            int W = imgHuellaScanner.Width;
            int H = imgHuellaScanner.Height;
            if (RecortarHuella)
            {
                X -= sensorW / 2;
                Y -= sensorH / 2;
                W = sensorW;
                H = sensorH;
                if (X < 0)
                    X = 0;
                if (Y < 0)
                    Y = 0;
                if (X + W > imgHuellaScanner.Width)
                    X = imgHuellaScanner.Width - W;
                if (Y + H > imgHuellaScanner.Height)
                    Y = imgHuellaScanner.Width - H;
            } else
            {
                X = 0;
                Y = 0;
            }

            float scaleX, scaleY;
            Image<Gray, byte> aux = null, aux1 = null, aux3 = null;
            //int maxMovePixeles = Y / 10;
            int maxMovePixeles = 1;
            int i, j, k;

            Image<Gray, byte> mask = new Image<Gray, byte>(W, H, new Gray(0));
            //Image<Gray, byte> mask = new Image<Gray, byte>(W, H, new Gray(255));
            //CvInvoke.Ellipse(mask, new Point(W / 2, H / 2), new Size(W / 2, H / 2), 0, 0, 360, new MCvScalar(0, 0, 0), -1, Emgu.CV.CvEnum.LineType.AntiAlias, 0); // -1 fill ellipse

            for (j = 0; j < maxMovePixeles; j++)
            {
                try
                {
                    aux3 = imgHuellaScanner.Copy(new Rectangle(X, Y + j * 10, W, H));
                    aux = new Image<Gray, byte>(aux3.Width, aux3.Height);
                    CvInvoke.FastNlMeansDenoising(aux3, aux);
                }
                catch (Exception ex)
                {
                    ;
                }
                huellasScanner.Add(new Huella(aux.Or(mask), Resolucion));
                float deltaScale = 0.05F;
                for (i = 0; i < MaxDeltaZoom; i++)
                {
                    //k = i;
                    for (k = 0; k < MaxDeltaZoom; k++)
                    {
                        scaleX = deltaScale * i;
                        scaleY = deltaScale * k;

                        if (i != 0 || k != 0)
                        {
                            int X1 = (int)((W * scaleX) / 2);
                            int Y1 = (int)((H * scaleY) / 2);
                            int W1 = (int)((W * (1 + scaleX)));
                            int H1 = (int)((H * (1 + scaleY)));

                            aux1 = aux.Resize(W1, H1, Inter.Linear).Copy(new Rectangle(X1, Y1, W, H));
                            huellasScanner.Add(new Huella(aux1.Or(mask), Resolucion));
                        }

                    }
                }
                if (j != 0)
                {
                    aux = imgHuellaScanner.Copy(new Rectangle(X, Y - j * 10, W, H));
                    huellasScanner.Add(new Huella(aux.Or(mask), Resolucion));
                    for (i = 0; i < MaxDeltaZoom; i++)
                    {
                        for (k = 0; k < MaxDeltaZoom; k++)
                        {
                            scaleX = deltaScale * i;
                            scaleY = deltaScale * k;

                            if (i != 0 && k != 0)
                            {
                                int X1 = (int)((W * scaleX) / 2);
                                int Y1 = (int)((H * scaleY) / 2);
                                int W1 = (int)((W * (1 + scaleX)));
                                int H1 = (int)((H * (1 + scaleY)));

                                aux1 = aux.Resize(W1, H1, Inter.Linear).Copy(new Rectangle(X1, Y1, W, H));
                                huellasScanner.Add(new Huella(aux1.Or(mask), Resolucion));
                            }
                        }
                    }
                }
            }
        }

        private Image<Gray, byte> cropAndZoom(Image<Gray, byte> img, float scaleX, float scaleY)
        {
            int W = sensorW;
            int H = sensorH;
            int X1 = (int)((W * scaleX) / 2);
            int Y1 = (int)((H * scaleY) / 2);
            int W1 = (int)((W * (1 + scaleX)));
            int H1 = (int)((H * (1 + scaleY)));


            /*
             * 
            Image<Gray, byte> aux = new Image<Gray, byte>(img.Width, img.Height);
            CvInvoke.FastNlMeansDenoising(img,aux);

            return aux.Resize(W1, H1, Inter.Linear).Copy(new Rectangle(X1, Y1, W, H));
            */
            return img.Resize(W1, H1, Inter.Linear).Copy(new Rectangle(X1, Y1, W, H));
        }

        protected List<Fmd> minutiae = new List<Fmd>();
        public List<IsoFormat> IsoList
        {
            get
            {
                List<IsoFormat> val = new List<IsoFormat>();
                foreach (Fmd m in minutiae)
                    val.Add(new IsoFormat(m.Bytes));
                return val;
            }
        }

        protected Thread minutiaeThread= null;

        protected void waithForMinutiaeThread()
        {
            if (minutiaeThread != null)
            {
                minutiaeThread.Join();
            }
        }

        protected void stopMinutiaeThread()
        {
            if (minutiaeThread != null)
            {
                minutiaeThread.Interrupt();
                minutiaeThread.Join();
                minutiaeThread = null;
            }
        }

        public void setMinutiaeScanner()
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            if (huellas.Count == 0)
            {
                generateHuellas();
                st.Stop();
                Console.WriteLine("generateHuellas -> {0} (seg)", st.Elapsed);
                st.Start();
            }
            List<Huella> huellasConTpl = new List<Huella>();
            CancellationTokenSource cts = new CancellationTokenSource();
            ParallelOptions po = new ParallelOptions();
            po.CancellationToken = cts.Token;
            po.MaxDegreeOfParallelism = System.Environment.ProcessorCount;
            
            /*minutiaeThread = new Thread((ThreadStart)delegate
            {*/
            System.Threading.Tasks.Parallel.ForEach(huellasScanner, po, huella =>
             {
                 try
                 {
                     po.CancellationToken.ThrowIfCancellationRequested();
                     DataResult<Fmd> resultConversion;
                     resultConversion = FeatureExtraction.CreateFmdFromRaw(ImgUtils.getRawData(huella.Img), 0, 0, huella.Img.Width, huella.Img.Height, DPIDef, Constants.Formats.Fmd.ISO);

                     if (resultConversion.ResultCode == Constants.ResultCode.DP_SUCCESS && resultConversion.Data != null)
                     {
                         lock (minutiae)
                         {
                             minutiae.Add(resultConversion.Data);
                             huellasConTpl.Add(new Huella(huella.Img, resultConversion.Data, resultConversion.Data.Views[0].Quality));
                             if (resultConversion.Data.ViewCount > 0)
                             {
                                 int q = resultConversion.Data.Views[0].Quality;
                                 if (q > QualityFromScanner)
                                     QualityFromScanner = q;
                             }
                         }
                     } else
                         ;
                 }
                 catch (Exception ex)
                 {
                     Console.WriteLine(ex.Message);
                 }
             });
            
            huellasScanner = huellasConTpl;
            if (huellasScanner.Count == 0)
            {
                throw new Exception("No se puede obtener una huella valida del scanner");
            }
        }

        public void saveHuellasScanner(string path)
        {
            saveHuellasScanner(path, "");
        }

        public void saveHuellasScanner(string path, string prefijo)
        {
            if (huellasScanner.Count == 0)
                generateHuellas();
            int i = 0;
            string fileName;
            foreach (Huella huella in huellasScanner)
            {
                if (ID == -1)
                    fileName = string.Format(@"{0}\{1}{2}-{3}.bmp", path, prefijo, getNumero(), i++);
                else
                    fileName = string.Format(@"{0}\{1}{2}-{3}.bmp", path, prefijo, ID, i++);
                huella.Img.Save(fileName);
            }
        }

        public void saveHuellasSensor(string path)
        {
            saveHuellasSensor(path, "");
        }

        public void saveHuellasSensor(string path, string prefijo)
        {
            int i = 0;
            string fileName;
            foreach (Huella huella in HuellasSensor)
            {
                if (ID == -1)
                    fileName = string.Format(@"{0}\{1}{2}-{3}.bmp", path, prefijo, getNumero(), i++);
                else
                    fileName = string.Format(@"{0}\{1}{2}-{3}.bmp", path, prefijo, ID, i++);
                huella.Img.Save(fileName);
            }
        }

        public int QualityFromSensor { get; private set; } = 0;
        public Huella LastFP
        {
            get
            {
                if (HuellasSensor.Count == 0)
                    return null;
                Huella lastFP = HuellasSensor[HuellasSensor.Count - 1];
                return lastFP;
            }
        }
        public BitmapSource WpfImgHuellaSensor
        {
            get
            {
                return ImgUtils.GetImageStream(LastFP.Img.Bitmap);
            }
        }

        public int OutDPI { get; private set; } = 0;

        public void loadFromSensor(Bitmap bmp)
        {
            loadFromSensor(bmp, DPIDef);
        }

        public void loadFromSensor(Bitmap bmp, int resolucion)
        {
            HuellasSensor.Add(new Huella(bmp, resolucion));
        }

        public void loadFromSensor(Fmd fmd, Bitmap bmp)
        {
            HuellasSensor.Add(new Huella(bmp, fmd));
            if (fmd.ViewCount == 0)
                QualityFromSensor = 0;
            else
                QualityFromSensor = fmd.Views[0].Quality;
        }

        private const int DPFJ_PROBABILITY_ONE = 0x7fffffff;
        private int thresholdScore = DPFJ_PROBABILITY_ONE * 1 / 10;
        public void setMaxThr(int maxThr)
        {
            thresholdScore = DPFJ_PROBABILITY_ONE * 1 / maxThr;
        }

        public bool Matches(Huella target, Huella prev)
        {
            try
            {
                CompareResult cr = Comparison.Compare(target.Tpl, 0, prev.Tpl, 0);

                return (cr.Score < thresholdScore);
            } catch (Exception ex)
            {
                return false;
            }
        }

        public bool Matches()
        {
            int min = DPFJ_PROBABILITY_ONE;
            int iMin = 0, i = 0;
            foreach (Fmd m in minutiae)
            {
                CompareResult cr = Comparison.Compare(LastFP.Tpl, 0, m, 0);
                if (min > cr.Score)
                {
                    min = cr.Score;
                    iMin = i;
                }
                i++;
            }
            long sc;
            try
            {
                sc = DPFJ_PROBABILITY_ONE / min;

            }
            catch (Exception)
            {
                sc = -1;
            }
            ScData = sc.ToString("X");
            return LastFP.OK = (min < thresholdScore);
        }

        public int MatchesAll()
        {
            int min;
            int iMin = 0, i = 0;
            foreach (Huella h in HuellasSensor)
            {
                min = DPFJ_PROBABILITY_ONE;
                iMin = i = 0;
                foreach (Fmd m in minutiae)
                {
                    CompareResult cr = Comparison.Compare(h.Tpl, 0, m, 0);
                    if (min > cr.Score)
                    {
                        min = cr.Score;
                        iMin = i;
                    }
                    i++;
                }
                Console.Write("{0}-{1},", DPFJ_PROBABILITY_ONE / min, iMin);
            }
            return 1;

        }

        #region DB
        public void saveToDB()
        {
            DB db = DBFactory.getDB();
            string dniStrSQL = db.getDNIInsertSQL();

            DbCommand cmd = (DbCommand)db.openConn().CreateCommand();
            cmd.CommandText = dniStrSQL;
            DbParameter param;

            param = cmd.CreateParameter();
            param.ParameterName = "@pdf_417_data";
            param.DbType = System.Data.DbType.String;
            param.Value = SerialData;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@dni";
            param.DbType = System.Data.DbType.String;
            param.Value = Numero;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@apellidos";
            param.DbType = System.Data.DbType.String;
            param.Value = Apellidos;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@nombres";
            param.DbType = System.Data.DbType.String;
            param.Value = Nombres;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@sexo";
            param.DbType = System.Data.DbType.String;
            param.Value = getSexo();
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@nacionalidad";
            param.DbType = System.Data.DbType.String;
            param.Value = Nacionalidad;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@fnacimiento";
            param.DbType = System.Data.DbType.Date;
            DateTime dt = DateTime.ParseExact(FechaNacimiento, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string strDate = dt.ToString("yyyy-MM-dd");
            param.Value = strDate;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@ok";
            param.DbType = System.Data.DbType.Int32;
            param.Value = OK ? 1 : 0;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@dpi";
            param.DbType = System.Data.DbType.Int32;
            param.Value = Resolucion;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@img1";
            param.DbType = System.Data.DbType.Binary;
            param.Value = ImgUtils.imageToByteArrayFmt(Anverso);
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@img2";
            param.DbType = System.Data.DbType.Binary;
            param.Value = ImgUtils.imageToByteArrayFmt(Reverso);
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@scdata";
            param.DbType = System.Data.DbType.String;
            param.Value = ScData;
            cmd.Parameters.Add(param);

            ID = db.insertLastId(cmd);

            string fpStrSQL =
                "INSERT INTO fp " +
                 "          (id_dni," +
                 "           img," +
                 "           tpl, " +
                 "           ok, " +
                 "           fmt, " +
                 "           version)" +
                 "   VALUES (@id_dni," +
                 "           @img, " +
                 "           @tpl, " +
                 "           @ok, " +
                 "           @fmt, " +
                 "           @version)";

            cmd = (DbCommand)db.openConn().CreateCommand();
            cmd.CommandText = fpStrSQL;

            param = cmd.CreateParameter();
            param.ParameterName = "@id_dni";
            param.DbType = System.Data.DbType.Int32;
            param.Value = ID;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@img";
            param.DbType = System.Data.DbType.Binary;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@tpl";
            param.DbType = System.Data.DbType.Binary;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@ok";
            param.DbType = System.Data.DbType.Int32;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@fmt";
            param.DbType = System.Data.DbType.Int32;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@version";
            param.DbType = System.Data.DbType.String;
            cmd.Parameters.Add(param);

            foreach (Huella h in HuellasSensor)
            {
                cmd.Parameters["@img"].Value = ImgUtils.imageToByteArray(h.Img.Bitmap);
                cmd.Parameters["@tpl"].Value = h.Tpl.Bytes;
                cmd.Parameters["@ok"].Value = h.OK ? 1 : 0;
                cmd.Parameters["@fmt"].Value = h.Tpl.Format;
                cmd.Parameters["@version"].Value = h.Tpl.Version;
                try
                {
                    db.insertLastId(cmd);
                } catch (Exception ex)
                {
                    ;
                }
            }

        }

        public static DNI loadByDNI(string dni)
        {
            string loadDniSQL =
                "SELECT id" +
                "  FROM dni" +
                " WHERE dni = @dni" +
                " ORDER BY id DESC";
            DNI retVal = null;
            DB db = null;
            try
            {
                db = DBFactory.getDB();
            }
            catch (Exception ex)
            {
                ;
            }

            DbCommand cmd = (DbCommand)db.openConn().CreateCommand();
            cmd.CommandText = loadDniSQL;
            DbParameter param;

            param = cmd.CreateParameter();
            param.ParameterName = "@dni";
            param.DbType = System.Data.DbType.String;
            param.Value = dni;
            cmd.Parameters.Add(param);

            DbDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                int id = (int)rdr["id"];
                rdr.Close();
                retVal = loadByID(id);
            } else
            {
                rdr.Close();
            }


            return retVal;
        }

        public static DNI loadByID(int id)
        {
            return loadByID(id, 0);
        }

        public static DNI loadByID(int id, int dpi)
        {
            DNI retVal = null;

            string loadDniSQL =
                "SELECT id" +
                "      ,ts" +
                "      ,id_user" +
                "      ,img1" +
                "      ,img2" +
                "      ,pdf_417_data" +
                "      ,dni" +
                "      ,apellidos" +
                "      ,nombres" +
                "      ,sexo" +
                "      ,nacionalidad" +
                "      ,fnacimiento" +
                "      ,ok" +
                "      ,dpi" +
                "      ,scdata" +
                "  FROM dni" +
                " WHERE id = @id" +
                " ORDER BY id DESC";

            DB db = DBFactory.getDB();

            DbCommand cmd = (DbCommand)db.openConn().CreateCommand();
            cmd.CommandText = loadDniSQL;
            DbParameter param;

            param = cmd.CreateParameter();
            param.ParameterName = "@id";
            param.DbType = System.Data.DbType.String;
            param.Value = id;
            cmd.Parameters.Add(param);

            DbDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                retVal = loadFromRdr(rdr);
                retVal.OutDPI = dpi;
            }

            return retVal;
        }

        public static List<int> loadIdsByDate(string nroDni, DateTime? desde, DateTime? hasta)
        {
            string loadDniSQL =
                "SELECT id" +
                "  FROM dni" +
                " WHERE 1=1";

            if (nroDni != null && nroDni != "")
                loadDniSQL += " AND dni = @nroDni";

            if (desde != null)
                loadDniSQL += " AND ts >= @desde";

            if (hasta != null)
                loadDniSQL += " AND ts <= @hasta";

            loadDniSQL += " ORDER BY id";

            DB db = DBFactory.getDB();

            DbCommand cmd = (DbCommand)db.openConn().CreateCommand();
            cmd.CommandText = loadDniSQL;
            DbParameter param;

            if (nroDni != null && nroDni != "")
            {
                param = cmd.CreateParameter();
                param.ParameterName = "@nroDni";
                param.DbType = System.Data.DbType.String;
                param.Value = nroDni;
                cmd.Parameters.Add(param);
            }

            if (desde != null)
            {
                param = cmd.CreateParameter();
                param.ParameterName = "@desde";
                param.DbType = System.Data.DbType.DateTime;
                param.Value = desde;
                cmd.Parameters.Add(param);
            }

            if (hasta != null)
            {
                param = cmd.CreateParameter();
                param.ParameterName = "@hasta";
                param.DbType = System.Data.DbType.DateTime;
                param.Value = hasta;
                cmd.Parameters.Add(param);
            }

            DbDataReader rdr = cmd.ExecuteReader();
            List<int> idList = new List<int>();
            while (rdr.Read())
            {
                idList.Add((int)rdr["id"]);
            }
            rdr.Close();

            return idList;
        }

        public static List<DNI> loadByDate(string nroDni, DateTime? desde, DateTime? hasta)
        {
            return loadByDate(nroDni, desde, hasta, 0);
        }
        public static List<DNI> loadByDate(string nroDni, DateTime? desde, DateTime? hasta, int dpi)
        {
            List<DNI> retVal = new List<DNI>();

            foreach (int id in loadIdsByDate(nroDni, desde, hasta))
            {
                DNI d = loadByID(id);
                retVal.Add(d);
            }

            return retVal;
        }



        public Bitmap getMiniImage(Bitmap bmp,int res,bool bw)
        {
            float scale = (float)res / (float)Resolucion;
            if (bw)
            {
                Image<Gray, byte> aux = new Image<Gray, byte>(bmp);
                return aux.Resize(scale, Inter.Linear).Bitmap;
            } else
            {
                Image<Rgb, Single> aux = new Image<Rgb, Single>(bmp);
                return aux.Resize(scale, Inter.Linear).Bitmap;
            }
        }

        protected static DNI loadFromRdr(DbDataReader rdr)
        {
            DNI retVal = DNIFactory.getNewInstance(rdr.GetString(5));
            byte[] imgBytes;
            imgBytes = (byte[])rdr["img1"];
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            retVal.Anverso = (Bitmap)tc.ConvertFrom(imgBytes);

            imgBytes = (byte[])rdr["img2"];
            retVal.Reverso = (Bitmap)tc.ConvertFrom(imgBytes);
            retVal.ID = (int)rdr["id"];
            retVal.Ts = (DateTime)rdr["ts"];
            retVal.IdUser = (string)rdr["id_user"];
            retVal.OK = (int)rdr["ok"] != 0;
            // OJO OJO OJO OJO OJO
            retVal.Resolucion = (int)rdr["dpi"];
            retVal.generateHuellas(true);
            rdr.Close();

            // Las huellas
            string loadFPSQL =
                    "SELECT id" +
                    "      ,id_dni " +
                    "      ,img " +
                    "      ,tpl " +
                    "      ,ok " +
                    "      ,fmt " +
                    "      ,version " +
                    "  FROM fp " +
                    " WHERE id_dni=@id_dni" +
                    " ORDER BY id";

            DB db = DBFactory.getDB();

            DbCommand cmd = (DbCommand)db.openConn().CreateCommand();
            cmd.CommandText = loadFPSQL;
            DbParameter param;

            param = cmd.CreateParameter();
            param.ParameterName = "@id_dni";
            param.DbType = System.Data.DbType.Int32;
            param.Value = retVal.ID;
            cmd.Parameters.Add(param);

            try
            {
                DbDataReader rdr2 = cmd.ExecuteReader();
                while (rdr2.Read())
                {
                    try
                    {
                        imgBytes = (byte[])rdr2["img"];
                        Bitmap bmp = (Bitmap)tc.ConvertFrom(imgBytes);
                        imgBytes = (byte[])rdr2["tpl"];
                        int fmt = (int)Constants.Formats.Fmd.ISO;
                        try
                        {
                            fmt = (int)rdr2["fmt"];
                        } catch (Exception e2)
                        {
                            ;
                        }
                        string version = "1.0.0";
                        try
                        {
                            version = (string)rdr2["version"];
                        }
                        catch (Exception e2)
                        {
                            ;
                        }

                        Fmd f = new Fmd(imgBytes, (int)Constants.Formats.Fmd.ISO, "1.0.0");
                        Huella h = new Huella(bmp, f);
                        h.OK = ((int)rdr2["ok"] != 0);
                        retVal.HuellasSensor.Add(h);
                    } catch (Exception e1)
                    {
                        ;
                    }

                }
                rdr2.Close();
            }
            catch (Exception ex) {
                ;
            }
            return retVal;
        }

        private static Dictionary<string, int> charDict = null;
        private static int getCharVal(string ch)
        {
            if (charDict == null)
            {
                // inicializo el diccionario
                charDict = new Dictionary<string, int>();
                for (char c = '0'; c <= '9'; c++)
                    charDict.Add(c.ToString(), (int)c - (int)'0');
                for (char c = 'A'; c <= 'Z'; c++)
                    charDict.Add(c.ToString(), (int)c - (int)'A' + 10);
            }
            try
            {
                return charDict[ch];
            } catch(Exception ex)
            {
                return 0;
            }
        }

        private static int getMult(int i)
        {
            switch (i%3)
            {
                case 0:
                    return 7;
                case 1:
                    return 3;
                case 2:
                    return 1;
                default:
                    throw new Exception("Error en la getMult");
            }
        }

        public static bool validOCR(string[] lineas)
        {
            if (lineas.Length != 3)
                return false;
            if (lineas[0].Length != 30 || lineas[1].Length != 30 || lineas[2].Length != 30)
                return false;

            // 1ra línea
            int i;
            if (lineas[0].Substring(0, 5) != "IDARG")
                return false;
            int dv = 0;
            for (i = 5; i < 30; i++)
            {
                string s = lineas[0].Substring(i, 1);
                if (s == "<")
                    break;
                dv += getCharVal(s) * getMult(i-5);
            }
            dv = dv % 10;
            if (dv != getCharVal(lineas[0].Substring(i + 1, 1)))
                return false;

            // 2da linea
            dv = 0;
            for (i = 0; i < 6; i++)
            {
                string s = lineas[1].Substring(i, 1);
                dv += getCharVal(s) * getMult(i);
            }
            dv %= 10;
            if (dv != getCharVal(lineas[1].Substring(6, 1)))
                return false;
            dv = 0;
            for (i = 8; i < 14; i++)
            {
                string s = lineas[1].Substring(i, 1);
                dv += getCharVal(s) * getMult(i-8);
            }
            dv %= 10;
            if (dv != getCharVal(lineas[1].Substring(14, 1)))
                return false;
            // TODO: averiguar contra que se compara el 4to digito verificador

            return true;
        }
    }
    #endregion

}
