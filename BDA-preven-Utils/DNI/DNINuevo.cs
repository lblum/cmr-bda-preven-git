﻿namespace BDAPreven.DNI
{
	class DNINuevo : DNI
	{
		public DNINuevo(string serialData)
			: base(serialData)
		{
            HuellaEnAnverso = false;
            FirmaEnAnverso = true;

            setRectHuella(5, 30, 65, 84);
            setRectFirma(28 ,40, 58, 82);
            setRectOCR(35, 50, 3, 83);
        }

        protected override string getNumero() { return getDataByPos(4); }
		protected override string getTipo() { return getDataByPos(5); }
		protected override string getApellidos() { return getDataByPos(1); }
		protected override string getNombres() { return getDataByPos(2); }
		protected override string getFechaNacimiento() { return getDataByPos(6); }
		public override string getSexo() { return getDataByPos(3); }

	}
}
