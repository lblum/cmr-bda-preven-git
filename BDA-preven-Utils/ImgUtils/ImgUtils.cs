﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace BDAPreven
{
    public class ImgUtils
    {
        public static System.Drawing.Bitmap ToWinFormsBitmap(BitmapSource bitmapsource)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(stream);

                using (var tempBitmap = new System.Drawing.Bitmap(stream))
                {
                    // According to MSDN, one "must keep the stream open for the lifetime of the Bitmap."
                    // So we return a copy of the new bitmap, allowing us to dispose both the bitmap and the stream.
                    return new System.Drawing.Bitmap(tempBitmap);
                }
            }
        }

        public static BitmapSource GetImageStream(System.Drawing.Image myImage)
        {
            var bitmap = new Bitmap(myImage);
            IntPtr bmpPt = bitmap.GetHbitmap();
            BitmapSource bitmapSource =
             System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                   bmpPt,
                   IntPtr.Zero,
                   Int32Rect.Empty,
                   BitmapSizeOptions.FromEmptyOptions());

            //freeze bitmapSource and clear memory to avoid memory leaks
            bitmapSource.Freeze();
            //DeleteObject(bmpPt);

            return bitmapSource;
        }

        public static byte[] getImgArray(BitmapSource src)
        {
            MemoryStream stream = new MemoryStream();
            BitmapEncoder enc = new PngBitmapEncoder();
            enc.Frames.Add(BitmapFrame.Create(src));
            enc.Save(stream);
            byte[] retVal = stream.ToArray();
            return retVal;
        }

        public static Image fromByteArray(byte[] bArray)
        {
            MemoryStream ms = new MemoryStream(bArray);
            BitmapSource bmpS = BitmapFrame.Create(ms);

            return ToWinFormsBitmap(bmpS);
        }

        public static BitmapSource ToWpfBitmap(Bitmap bitmap)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Bmp);

                stream.Position = 0;
                BitmapImage result = new BitmapImage();
                result.BeginInit();
                // According to MSDN, "The default OnDemand cache option retains access to the stream until the image is needed."
                // Force the bitmap to load right now so we can dispose the stream.
                result.CacheOption = BitmapCacheOption.OnLoad;
                result.StreamSource = stream;
                result.EndInit();
                result.Freeze();
                return result;
            }
        }

        public static Bitmap scaleBitmap(Bitmap bmp, int nPorc)
        {
            int newW;
            int newH;
            int newX;
            int newY;
            float r = (100F - (float)nPorc) / 100F;
            Bitmap bmp1, bmp2;

            newW = (int)((float)bmp.Width * r);
            newH = (int)((float)bmp.Height * r);

            newX = (bmp.Width - newW);
            newY = (bmp.Height - newH);
            bmp1 = bmp.Clone(new Rectangle(newX, newY, newW, newH), bmp.PixelFormat);
            bmp2 = new Bitmap(bmp1, new System.Drawing.Size(bmp.Width, bmp.Height));
            return bmp2;
        }


        public static Bitmap rawToBitmap(byte[] RawData, int Width, int Height)
        {
            byte[] rgbBytes = new byte[RawData.Length * 3];

            for (int i = 0; i <= RawData.Length - 1; i++)
            {
                rgbBytes[(i * 3)] = RawData[i];
                rgbBytes[(i * 3) + 1] = RawData[i];
                rgbBytes[(i * 3) + 2] = RawData[i];
            }

            Bitmap BMP = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);

            BitmapData data = BMP.LockBits(new Rectangle(0, 0, BMP.Width, BMP.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            for (int i = 0; i <= BMP.Height - 1; i++)
            {
                IntPtr p = new IntPtr(data.Scan0.ToInt64() + data.Stride * i);
                System.Runtime.InteropServices.Marshal.Copy(rgbBytes, i * BMP.Width * 3, p, BMP.Width * 3);
            }

            BMP.UnlockBits(data);

            return BMP;
        }

        public static byte[] bitmapToRaw(Bitmap bmp)
        {
            byte[] res = new byte[bmp.Height * bmp.Width];
            BitmapData srcData = null; ;
            try
            {
                srcData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
                int bpp = 4; ;
                unsafe
                {
                    int i = 0;
                    byte* srcPointer = (byte*)srcData.Scan0;
                    int f;
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        for (int x = 0; x < bmp.Width; x++)
                        {
                            f = srcPointer[1] + srcPointer[2] + srcPointer[3];
                            res[i++] = (byte)(f / 3);
                            srcPointer += bpp;
                        }
                    }
                }
            }
            catch (InvalidOperationException)
            {

            }
            finally
            {
                bmp.UnlockBits(srcData);
            }

            return res;
        }
        public static string ImgFmt = "PNG";
        public static int ImgQ = 80;

        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            return imageToByteArray(imageIn, "BMP");
        }

        public static byte[] imageToByteArrayFmt(System.Drawing.Image imageIn)
        {
            return imageToByteArray(imageIn, ImgFmt);
        }

        private static ImageCodecInfo GetEncoder(string format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatDescription.ToUpper() == format.ToUpper())
                {
                    return codec;
                }
            }
            return null;
        }

        public static byte[] imageToByteArray(System.Drawing.Image imageIn, string fmt)
        {
            if (imageIn == null)
                return null;
            MemoryStream ms = new MemoryStream();
            ImageCodecInfo enc = GetEncoder(fmt);
            EncoderParameters ecs = new EncoderParameters(1);
            System.Drawing.Imaging.Encoder myEncoder =
                System.Drawing.Imaging.Encoder.Quality;
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, ImgQ);
            ecs.Param[0] = myEncoderParameter;

            imageIn.Save(ms, enc, ecs);
            return ms.ToArray();
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static byte[] getRawData(Image<Gray, byte> img)
        {
            byte[] retVal = new byte[img.Width * img.Height];
            int i = 0;
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    retVal[i++] = img.Data[y, x, 0];
                }
            }
            return retVal;
        }



        public static System.Drawing.Point getCentroidFP(Image<Gray, Byte> img)
        {
            var moments = CvInvoke.Moments(img.Not().Mat, false);
            System.Drawing.Point c = new System.Drawing.Point((int)(moments.M10 / moments.M00), (int)(moments.M01 / moments.M00));
            return c;
        }

        public static System.Drawing.Point getCentroidTpl(Image<Gray, Byte> img, int dpi)
        {
            System.Drawing.Point res = getCentroidFP(img);
            /*
            DataResult<Fmd> resultConversion;
            resultConversion = FeatureExtraction.CreateFmdFromRaw(ImgUtils.getRawData(img), 0, 0, img.Width, img.Height, dpi, Constants.Formats.Fmd.ISO);

            if (resultConversion.ResultCode == Constants.ResultCode.DP_SUCCESS && resultConversion.Data != null)
            {
                IsoFormat ifo = new IsoFormat(resultConversion.Data.Bytes);
                res = ifo.Centroid;
            }
            */
            return res;

        }

        public static Bitmap scaleToWidth(Bitmap bmp, int w)
        {
            Image<Bgr, Byte> newImage = new Image<Bgr, Byte>(bmp);

            float scale = (float)w / (float)bmp.Width;
            Image<Bgr, Byte> retVal = newImage.Resize(scale, Inter.Linear);
            return retVal.Bitmap;
        }

    }
}
