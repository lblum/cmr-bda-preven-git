﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BDAPreven.DNI;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using BDAPreven.BIO;

namespace BDAPreven.Singleton
{
    public class SQLSDB : DB
    {
        public SQLSDB(string dbDSN) : base(dbDSN)
        {
        }

        protected override DbConnection makeConn(string dsn)
        {
            SqlConnection conn = new SqlConnection(dsn);
            return conn;
        }

        public override long insertLastId(DbCommand cmd)
        {
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public override string getDNIInsertSQL()
        {
            string strBase = base.getDNIInsertSQL();

            strBase = strBase.Replace("VALUES", "OUTPUT INSERTED.ID VALUES");

            return strBase;
        }



    }
}
