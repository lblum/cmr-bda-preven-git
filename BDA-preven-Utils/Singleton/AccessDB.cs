﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BDAPreven.DNI;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using BDAPreven.BIO;
using System.Data.OleDb;

namespace BDAPreven.Singleton
{
    public class AccessDB : DB
    {
        public AccessDB(string dbDSN) : base(dbDSN)
        {
        }

        protected override DbConnection makeConn(string dsn)
        {
            OleDbConnection conn = new OleDbConnection(dsn);
            return conn;
        }

        public override long insertLastId(DbCommand cmd)
        {
            cmd.ExecuteNonQuery();

            DbCommand cmdLI = Conn.CreateCommand();
            cmdLI.CommandText = "SELECT @@IDENTITY AS LastID"; ;

            DbDataReader rdr = cmdLI.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                int id = (int)rdr["LastID"];
                rdr.Close();
                return id;
            }
            else
            {
                rdr.Close();
            }

            return -1;
        }

        public override string getDNIInsertSQL()
        {
            return base.getDNIInsertSQL(); ;
        }

    }
}
