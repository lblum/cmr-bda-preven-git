﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BDAPreven.DNI;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Globalization;
using BDAPreven.BIO;
using System.Data;

namespace BDAPreven.Singleton
{
    class MySQLDB : DB
    {
        public MySQLDB(string dbDSN) : base(dbDSN)
        {
        }

        protected override DbConnection makeConn(string dsn)
        {
            MySqlConnection conn = new MySqlConnection(dsn);
            return conn;
        }

        public override long insertLastId(DbCommand cmd)
        {
            MySqlCommand mCmd = (MySqlCommand)cmd;
            mCmd.ExecuteNonQuery();
            return mCmd.LastInsertedId;
        }


    }
}
