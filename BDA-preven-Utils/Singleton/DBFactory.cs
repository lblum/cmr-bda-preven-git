﻿namespace BDAPreven.Singleton
{
    public class DBFactory
    {
        private static DB db = null;
        public static DB getDB()
        {
            if (db == null)
            {
                if (GlobalConfig.dbDriver == "mysql")
                    db = new MySQLDB(GlobalConfig.dbDSN);
                else if (GlobalConfig.dbDriver == "access")
                    db = new AccessDB(GlobalConfig.dbDSN);
                else
                    db = new SQLSDB(GlobalConfig.dbDSN);
            }

            return db;
        }
    }
}
