﻿using BDAPreven.DNI;
using System;
using System.Data;
using System.Data.Common;

namespace BDAPreven.Singleton
{
    public abstract class DB
    {
        private static object syncRoot = new Object();
        public DbConnection Conn { get; protected set; }

        public DB(string dbDSN)
        {
            Conn = makeConn(dbDSN);
            openConn();
        }

        protected abstract DbConnection makeConn(string dsn);

        public DbConnection openConn()
        {
            if (Conn.State != ConnectionState.Open)
                Conn.Open();
            return Conn;
        }

        public void closeConn()
        {
            if (Conn.State != ConnectionState.Closed)
                Conn.Close();
            if (Conn != null)
            {
                Conn = null;
            }
        }

        public static Object ToDbNull(Object o)
        {
            return o == null ? DBNull.Value : o;
        }

        public abstract long insertLastId(DbCommand cmd);

        public virtual string getDNIInsertSQL()
        {
            return
                "INSERT INTO dni" +
                "           (" + 
                "            pdf_417_data" +
                "           ,dni" +
                "           ,apellidos" +
                "           ,nombres" +
                "           ,sexo" +
                "           ,nacionalidad" +
                "           ,fnacimiento" +
                "           ,scdata" +
                "           ,ok" +
                "           ,dpi" +
                "           ,img1" +
                "           ,img2" +
                ")" +
                "    VALUES (" + 
                "            @pdf_417_data" +
                "           ,@dni" +
                "           ,@apellidos" +
                "           ,@nombres" +
                "           ,@sexo" +
                "           ,@nacionalidad" +
                "           ,@fnacimiento" +
                "           ,@scdata" +
                "           ,@ok" +
                "           ,@dpi" +
                "           ,@img1" +
                "           ,@img2" +                
                ")";
        }
    }


}
