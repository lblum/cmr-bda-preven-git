﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Drawing;
using SourceAFIS.General;

namespace SourceAFIS.Templates
{
    public class IsoFormat
    {
        // References:
        // http://www.italdata-roma.com/PDF/Norme%20ISO-IEC%20Minutiae%20Data%20Format%2019794-2.pdf
        // https://biolab.csr.unibo.it/fvcongoing/UI/Form/Download.aspx (ISO section, sample ZIP, ISOTemplate.pdf)
        //
        // Format (all numbers are big-endian):
        // 4B magic "FMR\0"
        // 4B version (ignored, set to " 20\0"
        // 4B total length (including header)
        // 2B rubbish (zeroed)
        // 2B image size in pixels X
        // 2B image size in pixels Y
        // 2B rubbish (pixels per cm X, set to 196 = 500dpi)
        // 2B rubbish (pixels per cm Y, set to 196 = 500dpi)
        // 1B rubbish (number of fingerprints, set to 1)
        // 1B rubbish (zeroed)
        // 1B rubbish (finger position, zeroed)
        // 1B rubbish (zeroed)
        // 1B rubbish (fingerprint quality, set to 100)
        // 1B minutia count
        // N*6B minutiae
        //      2B minutia position X in pixels
        //          2b (upper) minutia type (01 ending, 10 bifurcation, 00 other)
        //      2B minutia position Y in pixels (upper 2b ignored, zeroed)
        //      1B direction, compatible with SourceAFIS angles
        //      1B quality (ignored, zeroed)
        // 2B rubbish (extra data length, zeroed)
        // N*1B rubbish (extra data)

        public string Format { get; private set; }
        public string Version { get; private set; }
        public int Length { get; private set; }
        public int ImgWidth { get; private set; }
        public int ImgHeight { get; private set; }
        public int Quality { get; private set; }
        public int MinutiaCount { get; private set; }
        public Minutia[] Minutiae { get; private set; }
        public System.Drawing.Point Centroid;

        public byte[] Tpl;


        public IsoFormat(byte[] template)
        {
            Tpl = template;
            MemoryStream stream = new MemoryStream(template);
            BinaryReader reader = new BinaryReader(stream, Encoding.UTF8);

            // 4B magic "FMR\0"
            Format = new string(reader.ReadChars(4));

            // 4B version (ignored, set to " 20\0"
            Version = new string(reader.ReadChars(4));

            // 4B total length (including header)
            Length = IPAddress.NetworkToHostOrder(reader.ReadInt32());

            // 2B rubbish (zeroed)
            reader.ReadInt16();

            // 2B image size in pixels X
            ImgWidth = IPAddress.NetworkToHostOrder(reader.ReadInt16());

            // 2B image size in pixels Y
            ImgHeight = IPAddress.NetworkToHostOrder(reader.ReadInt16());

            // 2B rubbish (pixels per cm X, set to 196 = 500dpi)
            reader.ReadInt16();

            // 2B rubbish (pixels per cm Y, set to 196 = 500dpi)
            reader.ReadInt16();

            // 1B rubbish (number of fingerprints, set to 1)
            reader.ReadByte();

            // 1B rubbish (zeroed)
            reader.ReadByte();

            // 1B rubbish (finger position, zeroed)
            reader.ReadByte();

            // 1B rubbish (zeroed)
            reader.ReadByte();

            // 1B rubbish (fingerprint quality, set to 100)
            Quality = reader.ReadByte();

            // 1B minutia count
            MinutiaCount = reader.ReadByte();
            Minutiae = new Minutia[MinutiaCount];
            Centroid = new System.Drawing.Point(0, 0);

            for (int i = 0; i < MinutiaCount; ++i)
            {
                Minutia m = new Minutia();
                //      2B minutia position X in pixels
                //          2b (upper) minutia type (01 ending, 10 bifurcation, 00 other (considered ending))
                ushort xPacked = (ushort)IPAddress.NetworkToHostOrder(reader.ReadInt16());
                m.X = xPacked & (ushort)0x3fff;
                switch (xPacked & (ushort)0xc000)
                {
                    case 0x4000: m.Type = MinutiaType.Ending; break;
                    case 0x8000: m.Type = MinutiaType.Bifurcation; break;
                    case 0: m.Type = MinutiaType.Other; break;
                }
                //      2B minutia position Y in pixels (upper 2b ignored, zeroed)
                m.Y = ImgHeight - 1 - ((ushort)IPAddress.NetworkToHostOrder(reader.ReadInt16()) & (ushort)0x3fff);
                //      1B direction, compatible with SourceAFIS angles
                m.Angle = reader.ReadByte();
                //      1B quality (ignored, zeroed)
                m.Quality = reader.ReadByte();
                Minutiae[i] = m;
                Centroid.X += m.X;
                Centroid.Y += m.Y;
            }
            if (MinutiaCount > 0)
            {
                Centroid.X /= MinutiaCount;
                Centroid.Y /= MinutiaCount;
            }

            // 2B rubbish (extra data length, zeroed)
            // N*1B rubbish (extra data)

        }

        public byte[] getBytes()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8);

            checked
            {
                // 4B magic "FMR\0"
                writer.Write(Format.ToCharArray());

                // 4B version (ignored, set to " 20\0"
                writer.Write(Version.ToCharArray());

                // 4B total length (including header, will be updated later)
                writer.Write(0);

                // 2B rubbish (zeroed)
                writer.Write((short)0);

                // 2B image size in pixels X
                writer.Write(IPAddress.HostToNetworkOrder((short)ImgWidth));

                // 2B image size in pixels Y
                writer.Write(IPAddress.HostToNetworkOrder((short)ImgHeight));

                // 2B rubbish (pixels per cm X, set to 196 = 500dpi)
                writer.Write(IPAddress.HostToNetworkOrder((short)196));

                // 2B rubbish (pixels per cm Y, set to 196 = 500dpi)
                writer.Write(IPAddress.HostToNetworkOrder((short)196));

                // 1B rubbish (number of fingerprints, set to 1)
                writer.Write((byte)1);

                // 1B rubbish (zeroed)
                writer.Write((byte)0);

                // 1B rubbish (finger position, zeroed)
                writer.Write((byte)0);

                // 1B rubbish (zeroed)
                writer.Write((byte)0);

                // 1B rubbish (fingerprint quality, set to 100)
                writer.Write((byte)100);

                // 1B minutia count
                writer.Write((byte)Minutiae.Length);

                // N*6B minutiae
                foreach (Minutia minutia in Minutiae)
                {
                    //      2B minutia position X in pixels
                    //          2b (upper) minutia type (01 ending, 10 bifurcation, 00 other (considered ending))
                    int x = minutia.X;
                    int type;
                    switch (minutia.Type)
                    {
                        case MinutiaType.Ending: type = 0x4000; break;
                        case MinutiaType.Bifurcation: type = 0x8000; break;
                        case MinutiaType.Other: type = 0; break;
                        default: type = 0; break;
                    }
                    writer.Write(IPAddress.HostToNetworkOrder(unchecked((short)(x | type))));

                    //      2B minutia position Y in pixels (upper 2b ignored, zeroed)
                    int y = ImgHeight - minutia.Y - 1;
                    writer.Write(IPAddress.HostToNetworkOrder((short)y));

                    //      1B direction, compatible with SourceAFIS angles
                    writer.Write((byte)minutia.Angle);

                    //      1B quality (ignored, zeroed)
                    writer.Write((byte)minutia.Quality);
                }

                // 2B rubbish (extra data length, zeroed)
                // N*1B rubbish (extra data)
                writer.Write((short)0);
            }

            writer.Close();

            // update length
            byte[] template = stream.ToArray();
            BitConverter.GetBytes(IPAddress.HostToNetworkOrder(template.Length)).CopyTo(template, 8);

            IsoFormat ii = new IsoFormat(template);
            return template;
        }

        public IsoFormat zoom(float scaleX , float scaleY)
        {
            IsoFormat f = new IsoFormat(Tpl);

            List<Minutia> efList = new List<Minutia>();

            foreach ( Minutia m in f.Minutiae)
            {
                m.X = (int)((float)m.X * (1+scaleX));
                m.Y = (int)((float)m.Y * (1+scaleY));
                if ( m.Y>=0 && m.X >=0 && m.X<= ImgWidth && m.Y<=ImgHeight)
                {
                    ///// OJO!!!!!!!
                    double alfa = (double)m.Angle * 2 * Math.PI / 256.0;
                    double sin =  Math.Sin(alfa);
                    double cos =  Math.Cos(alfa);

                    m.Angle = Angle.ToByte(Angle.Atan((1 + scaleX) * cos, (1 + scaleY) * sin));
                    efList.Add(m);
                }
            }
            f.MinutiaCount = efList.Count;
            f.Minutiae = efList.ToArray();

            return new IsoFormat(f.getBytes());
        }

    }

    public class Minutia
    {
        public MinutiaType Type;
        public int X;
        public int Y;
        public int Angle;
        public int Quality;
    }

    public enum MinutiaType
    {
        Ending = 0,
        Bifurcation = 1,
        Other = 2
    }
}
