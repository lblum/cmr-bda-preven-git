﻿using System;
using ADOX;
using System.Data.OleDb;

namespace BDA_preven_access
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Catalog cat = new Catalog();
            //CatalogClass cat = new CatalogClass();


            string Provider = "Provider=Microsoft.Jet.OLEDB.4.0;" +
   @"Data Source=..\..\..\BDA-preven-ng\bin\debug\BDA-preven.mdb;" +
   "Jet OLEDB:Engine Type=5";
            try
            {
                cat.Create(Provider);
                cat = null;

            }
            catch (Exception ex)
            {

            }
            OleDbConnection conn = new OleDbConnection(Provider);
            conn.Open();
            string dropTable = "drop table dni";
            OleDbCommand cmdD = new OleDbCommand(dropTable, conn);
            try
            {
                cmdD.ExecuteNonQuery();
            } catch(Exception ex)
            {

            }
            dropTable = "drop table fp";
            cmdD = new OleDbCommand(dropTable, conn);
            try
            {
                cmdD.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }

            string tblCreate = "create table [dni](";
            tblCreate += "  [id] counter identity(1,1)";
            tblCreate += " ,[ts] datetime default now()";
            tblCreate += " ,[id_user] varchar(255) default 'Admin'";
            tblCreate += " ,[img1] longbinary null";
            tblCreate += " ,[img2] longbinary null";
            tblCreate += " ,[pdf_417_data] varchar(255) null";
            tblCreate += " ,[dni] varchar(255) null";
            tblCreate += " ,[apellidos] varchar(255) null";
            tblCreate += " ,[nombres] varchar(255) null";
            tblCreate += " ,[sexo] varchar(255) null";
            tblCreate += " ,[nacionalidad] varchar(255) null";
            tblCreate += " ,[fnacimiento] date null";
            tblCreate += " ,[ok] int null";
            tblCreate += " ,[scdata] longtext null";
            tblCreate += ")";
            OleDbCommand cmd = new OleDbCommand(tblCreate, conn);
            cmd.ExecuteNonQuery();

            tblCreate = "create table [fp](";
            tblCreate += "  [id] counter identity(1,1)";
            tblCreate += " ,[id_dni] int not null";
            tblCreate += " ,[img] longbinary null";
            tblCreate += " ,[tpl] longbinary null";
            tblCreate += " ,[ok] int not null default 1";
            tblCreate += " ,[fmt] int null";
            tblCreate += " ,[version] longtext null";
            tblCreate += ")";
            cmd = new OleDbCommand(tblCreate, conn);
            cmd.ExecuteNonQuery();

        }
    }
}
