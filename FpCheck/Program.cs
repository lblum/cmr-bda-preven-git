﻿using BDAPreven.DNI;
using BDAPreven.Singleton;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing.Imaging;

namespace FpCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            //getImgs();
            //cmpImgs();
            //zoomTpls();
            /*for (int i = 0; i < 255; i++)
            {

            }
            */
            parsePDF(@".\data\DOC075.pdf",@"c:\tmp");
            //parsePDF(@"./DOC076.pdf", @"c:\tmp");
            if (System.Diagnostics.Debugger.IsAttached)
                Console.ReadLine();

        }

        protected static void parsePDF(string sourcePdf, string outputPath)
        {
            PdfReader pdf = new PdfReader(sourcePdf);
            RandomAccessFileOrArray raf = new iTextSharp.text.pdf.RandomAccessFileOrArray(sourcePdf);
            try
            {
                for (int pageNumber = 1; pageNumber <= pdf.NumberOfPages; pageNumber++)
                {
                    PdfDictionary pg = pdf.GetPageN(pageNumber);
                    PdfDictionary res =
                      (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));
                    PdfDictionary xobj =
                      (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
                    if (xobj != null)
                    {
                        foreach (PdfName name in xobj.Keys)
                        {
                            PdfObject obj = xobj.Get(name);
                            if (obj.IsIndirect())
                            {
                                PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);
                                PdfName type =
                                  (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));
                                if (PdfName.IMAGE.Equals(type))
                                {

                                    int XrefIndex = Convert.ToInt32(((PRIndirectReference)obj).Number.ToString(System.Globalization.CultureInfo.InvariantCulture));
                                    PdfObject pdfObj = pdf.GetPdfObject(XrefIndex);
                                    PdfStream pdfStrem = (PdfStream)pdfObj;
                                    byte[] bytes = PdfReader.GetStreamBytesRaw((PRStream)pdfStrem);
                                    if ((bytes != null))
                                    {
                                        using (System.IO.MemoryStream memStream = new System.IO.MemoryStream(bytes))
                                        {
                                            memStream.Position = 0;
                                            System.Drawing.Image img = System.Drawing.Image.FromStream(memStream);
                                            // must save the file while stream is open.
                                            if (!Directory.Exists(outputPath))
                                                Directory.CreateDirectory(outputPath);

                                            string path = Path.Combine(outputPath, String.Format(@"{0}.jpg", pageNumber));
                                            System.Drawing.Imaging.EncoderParameters parms = new System.Drawing.Imaging.EncoderParameters(1);
                                            parms.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression, 0);
// GetImageEncoder is found below this method
                                            System.Drawing.Imaging.ImageCodecInfo jpegEncoder = GetImageEncoder("JPEG");
                                            img.Save(path, jpegEncoder, parms);
                                            break;

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch
            {
                throw;
            }
            finally
            {
                pdf.Close();
            }



        }
        public static System.Drawing.Imaging.ImageCodecInfo GetImageEncoder(string imageType)
        {
            imageType = imageType.ToUpperInvariant();



            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if (info.FormatDescription == imageType)
                {
                    return info;
                }
            }

            return null;
        }

        protected static void zoomTpls()
        {
            GlobalConfig.dbDriver = ConfigurationSettings.AppSettings["dbDriver"];
            GlobalConfig.dbDSN = ConfigurationSettings.AppSettings["dbDSN"];
            List<DNI> dniList = DNI.loadByDate(null, null, null);
            DNI.MaxDeltaZoom = Int32.Parse(ConfigurationSettings.AppSettings["MaxDeltaZoom"]);
            foreach (DNI d in dniList)
            {
                d.getScaledTpl();
            }
        }

        protected static void getImgs()
        {
            GlobalConfig.dbDriver = ConfigurationSettings.AppSettings["dbDriver"];
            GlobalConfig.dbDSN = ConfigurationSettings.AppSettings["dbDSN"];
            //List<DNI> dniList = DNI.loadByDate(null, null, null);
            //DNI.MaxDeltaZoom = Int32.Parse(ConfigurationSettings.AppSettings["MaxDeltaZoom"]);
            //foreach (DNI d in dniList)
            DNI d = DNI.loadByID(78);
            {
                Bitmap anv = d.Anverso;
                Bitmap rev = d.Reverso;
                /*
                d.Anverso = d.getMiniImage(anv, 150, false);
                d.Reverso = d.getMiniImage(rev, 150, false);
                d.saveToDB();
                d.Anverso = d.getMiniImage(anv, 300, false);
                d.Reverso = d.getMiniImage(rev, 300, false);
                d.saveToDB();
                */
                d.Anverso = d.getMiniImage(anv, 150, true);
                d.Reverso = d.getMiniImage(rev, 150, true);
                d.saveToDB();
                d.Anverso = d.getMiniImage(anv, 300, true);
                d.Reverso = d.getMiniImage(rev, 300, true);
                d.saveToDB();
                d.Anverso = d.getMiniImage(anv, 600, true);
                d.Reverso = d.getMiniImage(rev, 600, true);
                d.saveToDB();
                /*
                var api = OcrApi.Create();
                api.Init(Patagames.Ocr.Enums.Languages.English);
                var rend = OcrTextRenderer.Create("qq.qq");
                OcrPix qq = OcrPix.FromBitmap(d.Anverso);
                api.ProcessPage(qq, null, 0, rend);
                
                var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
                engine.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ<>1234567890");
                
                Pix pix = PixConverter.ToPix(d.Reverso);
                
                var page = engine.Process(pix);
                var text = page.GetText();
                Console.WriteLine("Mean confidence: {0}", page.GetMeanConfidence());



                Console.WriteLine("Text (GetText): \r\n{0}", text);

                Console.WriteLine("Text (iterator):");

                using (var iter = page.GetIterator())

                {

                    iter.Begin();



                    do

                    {

                        do

                        {

                            do

                            {

                                do

                                {

                                    if (iter.IsAtBeginningOf(PageIteratorLevel.Block))

                                    {

                                        Console.WriteLine("<BLOCK>");

                                    }



                                    Console.Write(iter.GetText(PageIteratorLevel.Word));

                                    Console.Write(" ");



                                    if (iter.IsAtFinalOf(PageIteratorLevel.TextLine, PageIteratorLevel.Word))

                                    {

                                        Console.WriteLine();

                                    }

                                } while (iter.Next(PageIteratorLevel.TextLine, PageIteratorLevel.Word));



                                if (iter.IsAtFinalOf(PageIteratorLevel.Para, PageIteratorLevel.TextLine))

                                {

                                    Console.WriteLine();

                                }

                            } while (iter.Next(PageIteratorLevel.Para, PageIteratorLevel.TextLine));

                        } while (iter.Next(PageIteratorLevel.Block, PageIteratorLevel.Para));

                    } while (iter.Next(PageIteratorLevel.Block));
                }
                */
                Console.WriteLine("id->{0}", d.ID);
                //d.RecortarHuella = false;
                /*
                d.saveHuellasScanner(@"c:\tmp\bmp", "recorte-");
                d.saveHuellasSensor(@"c:\tmp\bmp", "sensor-");
                */
            }
        }

        protected static void cmpImgs()
        {
            GlobalConfig.dbDriver = ConfigurationSettings.AppSettings["dbDriver"];
            GlobalConfig.dbDSN = ConfigurationSettings.AppSettings["dbDSN"];
            DNI.MaxDeltaZoom = Int32.Parse(ConfigurationSettings.AppSettings["MaxDeltaZoom"]);
            //foreach (DNI d in dniList)
            {
                DNI d = DNI.loadByID(4);
                try
                {
                    d.setMinutiaeScanner();

                    var z = d.IsoList;
                    Console.Write("id->{0}/", d.ID);
                    d.MatchesAll();
                    Console.WriteLine();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }


        }
    }
}
